/**
 * Automatically generated file. DO NOT MODIFY
 */
package id.co.pt_gema_solusindo_utama.cak_surdin;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "id.co.pt_gema_solusindo_utama.cak_surdin";
  public static final String BUILD_TYPE = "release";
  public static final int VERSION_CODE = 48;
  public static final String VERSION_NAME = "3.8.2";
}
