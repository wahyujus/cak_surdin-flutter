import 'dart:io';

import 'package:cak_surdin/models/CariSuratModel.dart';
import 'package:cak_surdin/models/DummyModel.dart';
import 'package:cak_surdin/models/LoginModel.dart';
import 'package:cak_surdin/models/RegisterModel.dart';
import 'package:cak_surdin/models/SuratMasukDetailModel.dart';
import 'package:cak_surdin/models/SuratMasukModel.dart';
import 'package:cak_surdin/models/TandaTerimaModel.dart';
import 'package:file_picker/file_picker.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart' as mime;

class RestServices {
  static var client = http.Client();
  static var baseUrl = "http://www.smspaskot.com/index.php/android/";

  // post request

  static Future<LoginModel> postLogin(String username, String password) async {
    var request = http.MultipartRequest(
      'POST',
      Uri.parse(baseUrl + "login"),
    );

    request.fields["username"] = username;
    request.fields["password"] = password;

    var response = await request.send();
    var responseData = await response.stream.toBytes();
    var responseString = String.fromCharCodes(responseData);
    print('register = $responseString');
    if (response.statusCode == 200) {
      var result = loginModelFromJson(responseString);
      return result;
    } else {
      var result = loginModelFromJson(responseString);
      print('false postlogin');
      return result;
    }
  }

  static Future<RegisterModel> postRegister(String username, String namaLengkap,
      String password, String no_tlp, String nip) async {
    var request = http.MultipartRequest(
      'POST',
      Uri.parse(baseUrl + "register"),
    );

    request.fields["NamaLogin"] = username;
    request.fields["NamaUser"] = namaLengkap;
    request.fields["Pwd"] = password;
    request.fields["no_tlp"] = no_tlp;
    request.fields["NipUser"] = nip;
    request.fields["SatKerID"] = '01';
    request.fields["is_external"] = '1';

    var response = await request.send();
    var responseData = await response.stream.toBytes();
    var responseString = String.fromCharCodes(responseData);
    print('register = $responseString');
    if (response.statusCode == 200) {
      var result = registerModelFromJson(responseString);
      return result;
    } else {
      var result = registerModelFromJson(responseString);
      print('false postlogin');
      return result;
    }
  }

  static Future<String> postResi(
    String userLoginID,
    String namaPengirim,
    String suratMasukID,
    String nomorSurat,
    String instansi,
    String type,
    String typeSurat,
    List<String> penerima,
    List<String> surat,
    File ttd,
  ) async {
    var request = http.MultipartRequest(
      'POST',
      Uri.parse(baseUrl + "tanda_terima_insert"),
    );

    request.fields["UserLoginID"] = userLoginID;
    request.fields["nama_pengirim"] = namaPengirim;
    request.fields["SuratMasukID"] = suratMasukID;
    request.fields["nomor_surat"] = nomorSurat;
    request.fields["instansi"] = instansi;
    request.fields["type"] = type;
    request.fields["type_surat"] = typeSurat;

    if (penerima.isNotEmpty) {
      // var path = penerima.map((e) => e.path);
      // var fileName = penerima.map((e) => e.name);
      // List<String> pathString = path.toList();
      // List<String> fileNameString = fileName.toList();
      var count = 0;

      for (final i in penerima) {
        var fotoPenerima = await http.MultipartFile.fromPath(
          "penerima_$count",
          i,
          filename: DateTime.now().toString() + '$count',
        );
        request.files.add(fotoPenerima);
        count++;
      }
    }

    // if (surat.isNotEmpty) {
    //   var path = surat.map((e) => e.path);
    //   var fileName = surat.map((e) => e.name);
    //   List<String> pathString = path.toList();
    //   List<String> fileNameString = fileName.toList();
    //   var count = 0;

    //   for (final i in pathString) {
    //     var fotoSurat = await http.MultipartFile.fromPath(
    //       "surat_$count",
    //       i,
    //       filename: fileNameString.elementAt(count),
    //     );
    //     request.files.add(fotoSurat);
    //     count++;
    //   }
    // }

    if (ttd.path.isNotEmpty) {
      var fotoTTD = await http.MultipartFile.fromPath(
        "ttd",
        ttd.path,
        filename: DateTime.now().toString(),
        // contentType: mime.MediaType("media", "jpg"),
      );
      request.files.add(fotoTTD);
    }

    var response = await request.send();
    var responseData = await response.stream.toBytes();
    var responseString = String.fromCharCodes(responseData);
    print('tanda_terima_insert = $responseString');
    if (response.statusCode == 200) {
      var result = tandaTerimaModelFromJson(responseString);
      return result.message;
    } else {
      var result = tandaTerimaModelFromJson(responseString);
      return result.message;
    }
  }

  static Future<bool> deleteResi(String id) async {
    var response = await client.post(
      Uri.parse(baseUrl + 'tanda_terima_delete/$id'),
    );

    if (response.statusCode == 200) {
      var str = response.body;
      print('deleteResi : ${str.length}');
      return true;
    } else {
      print('deleteResi : $response');
      return false;
    }
  }

  // get request

  static Future<CariSuratModel> fetchCariSurat(String nomerSurat) async {
    var response = await client.get(
      Uri.parse(baseUrl + 'data_surat?param=&nomor_surat=$nomerSurat'),
    );

    if (response.statusCode == 200) {
      var str = response.body;
      print('fetchCariSurat : ${str.length}');
      return cariSuratModelFromJson(str);
    } else {
      print('fetchCariSurat : $response');
      return null;
    }
  }

  static Future<SuratMasukModel> fetchSuratMasuk(String typeSurat) async {
    var response = await client.get(
      Uri.parse(baseUrl + 'surat_filter?param=&jenis_surat=$typeSurat'),
    );

    if (response.statusCode == 200) {
      var str = response.body;
      print('fetchSuratMasuk : ${str.length}');
      return suratMasukModelFromJson(str);
    } else {
      print('fetchSuratMasuk : $response');
      return null;
    }
  }

  static Future<SuratMasukModel> suratMasukFilter(String nomorSurat,
      String perihal, String tanggal, String jenisSurat) async {
    var response = await client.get(
      Uri.parse(baseUrl +
          'surat_filter?param=&nomor_surat=$nomorSurat&jenis_surat=$jenisSurat&perihal=$perihal&tanggal_akhir=$tanggal&tanggal_awal=$tanggal'),
    );

    if (response.statusCode == 200) {
      var str = response.body;
      print('fetchSuratMasuk : ${str.length}');
      return suratMasukModelFromJson(str);
    } else {
      print('fetchSuratMasuk : $response');
      return null;
    }
  }

  static Future<SuratMasukDetailModel> fetchSuratMasukDetail(
      String suratMasukID,
      String userLoginId,
      String nomorSurat,
      String type) async {
    var response = await client.get(
      Uri.parse(baseUrl +
          'tanda_terima_get?param=&SuratMasukID=$suratMasukID&user_login_id=$userLoginId&nomor_surat=$nomorSurat&type=$type'),
    );

    if (response.statusCode == 200) {
      var str = response.body;
      print('fetchSuratMasukDetail : ${str.length}');
      return suratMasukDetailModelFromJson(str);
    } else {
      print('fetchSuratMasukDetail : $response');
      return null;
    }
  }

  //profile
  static Future<bool> updateProfile(
    String userLoginID,
    String namaLogin,
    String namaUser,
    String password,
    String no_tlp,
  ) async {
    var request = http.MultipartRequest(
      'POST',
      Uri.parse(baseUrl + "user_update"),
    );

    request.fields["UserLoginID"] = userLoginID;
    request.fields["NamaLogin"] = namaLogin;
    request.fields["NamaUser"] = namaUser;
    request.fields["Pwd"] = password;
    request.fields["no_tlp"] = no_tlp;

    var response = await request.send();
    var responseData = await response.stream.toBytes();
    var responseString = String.fromCharCodes(responseData);
    print('register = $responseString');
    if (response.statusCode == 200) {
      return true;
    } else {
      print('false postlogin');
      return false;
    }
  }

  static Future<DummyModel> fetchDummy() async {
    var response = await client.get(
      Uri.parse(baseUrl + 'get_satker'),
    );

    if (response.statusCode == 200) {
      var str = response.body;
      print('fetchdummy : ${str.length}');
      return dummyModelFromJson(str);
    } else {
      print('fetchdummy : $response');
      return null;
    }
  }

  //get

  static Future<SuratMasukDetailModel> fetchResiTandaTerimaDetail(
      String userLoginId, String type) async {
    var response = await client.get(
      Uri.parse(baseUrl +
          'tanda_terima_get?param=&user_login_id=$userLoginId&type=$type'),
    );

    if (response.statusCode == 200) {
      var str = response.body;
      print('fetchSuratMasukDetail : ${str.length}');
      return suratMasukDetailModelFromJson(str);
    } else {
      print('fetchSuratMasukDetail : $response');
      return SuratMasukDetailModel();
    }
  }

  //post

  static Future<bool> resiTandaTerima(
    String userLoginID,
    String namaPengirim,
    String nomorSurat,
    String type,
    String instansi,
    List<String> penerima,
    List<String> surat,
    File ttd,
  ) async {
    var request = http.MultipartRequest(
      'POST',
      Uri.parse(baseUrl + "tanda_terima_insert"),
    );

    request.fields["UserLoginID"] = userLoginID;
    request.fields["SuratMasukID"] = '';
    request.fields["nama_pengirim"] = namaPengirim;
    request.fields["nomor_surat"] = nomorSurat;
    request.fields["instansi"] = instansi;
    request.fields["type"] = type;

    if (penerima != null) {
      // var path = penerima.map((e) => e.path);
      // var fileName = penerima.map((e) => e.name);
      // List<String> pathString = path.toList();
      // List<String> fileNameString = fileName.toList();
      var count = 0;

      for (final i in penerima) {
        var fotoPenerima = await http.MultipartFile.fromPath(
          "penerima_$count",
          i,
          filename: DateTime.now().toString() + '$count',
        );
        request.files.add(fotoPenerima);
        count++;
      }
    }

    if (surat != null) {
      print(surat);
      // var path = surat.map((e) => e.path);
      // var fileName = surat.map((e) => e.name);
      // List<String> pathString = path.toList();
      // List<String> fileNameString = fileName.toList();
      var count = 0;

      for (final i in surat) {
        var fotoSurat = await http.MultipartFile.fromPath(
          "surat_$count",
          i,
          filename: DateTime.now().toString() + '$count',
        );
        request.files.add(fotoSurat);
        count++;
      }
    }

    if (ttd.path != null) {
      var fotoTTD = await http.MultipartFile.fromPath(
        "ttd",
        ttd.path,
        filename: DateTime.now().toString(),
        // contentType: mime.MediaType("media", "jpg"),
      );
      request.files.add(fotoTTD);
    }

    var response = await request.send();
    var responseData = await response.stream.toBytes();
    var responseString = String.fromCharCodes(responseData);
    print('tanda_terima_insert = $responseString');
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }
}
