// To parse this JSON data, do
//
//     final registerModel = registerModelFromJson(jsonString);

import 'dart:convert';

RegisterModel registerModelFromJson(String str) =>
    RegisterModel.fromJson(json.decode(str));

String registerModelToJson(RegisterModel data) => json.encode(data.toJson());

class RegisterModel {
  RegisterModel({
    this.data,
    this.message,
    this.error,
  });

  Data data;
  String message;
  bool error;

  factory RegisterModel.fromJson(Map<String, dynamic> json) => RegisterModel(
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
        message: json["message"] == null ? null : json["message"],
        error: json["error"] == null ? null : json["error"],
      );

  Map<String, dynamic> toJson() => {
        "data": data == null ? null : data.toJson(),
        "message": message == null ? null : message,
        "error": error == null ? null : error,
      };
}

class Data {
  Data({
    this.userLoginId,
    this.namaLogin,
    this.namaUser,
    this.nipUser,
    this.satKerId,
    this.noTlp,
    this.satker,
    this.jenis,
    this.isExternal,
    this.tu,
  });

  String userLoginId;
  String namaLogin;
  String namaUser;
  String nipUser;
  String satKerId;
  String noTlp;
  String satker;
  String jenis;
  bool isExternal;
  String tu;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        userLoginId: json["UserLoginID"] == null ? null : json["UserLoginID"],
        namaLogin: json["NamaLogin"] == null ? null : json["NamaLogin"],
        namaUser: json["NamaUser"] == null ? null : json["NamaUser"],
        nipUser: json["NipUser"] == null ? null : json["NipUser"],
        satKerId: json["SatKerID"] == null ? null : json["SatKerID"],
        noTlp: json["no_tlp"] == null ? null : json["no_tlp"],
        satker: json["Satker"] == null ? null : json["Satker"],
        jenis: json["Jenis"] == null ? null : json["Jenis"],
        isExternal: json["is_external"] == null ? null : json["is_external"],
        tu: json["tu"] == null ? null : json["tu"],
      );

  Map<String, dynamic> toJson() => {
        "UserLoginID": userLoginId == null ? null : userLoginId,
        "NamaLogin": namaLogin == null ? null : namaLogin,
        "NamaUser": namaUser == null ? null : namaUser,
        "NipUser": nipUser == null ? null : nipUser,
        "SatKerID": satKerId == null ? null : satKerId,
        "no_tlp": noTlp == null ? null : noTlp,
        "Satker": satker == null ? null : satker,
        "Jenis": jenis == null ? null : jenis,
        "is_external": isExternal == null ? null : isExternal,
        "tu": tu == null ? null : tu,
      };
}
