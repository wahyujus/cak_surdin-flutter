// To parse this JSON data, do
//
//     final suratMasukModel = suratMasukModelFromJson(jsonString);

import 'dart:convert';

SuratMasukModel suratMasukModelFromJson(String str) =>
    SuratMasukModel.fromJson(json.decode(str));

String suratMasukModelToJson(SuratMasukModel data) =>
    json.encode(data.toJson());

class SuratMasukModel {
  SuratMasukModel({
    this.list,
    this.message,
    this.error,
  });

  List<ListElement> list;
  String message;
  bool error;

  factory SuratMasukModel.fromJson(Map<String, dynamic> json) =>
      SuratMasukModel(
        list: List<ListElement>.from(
            json["list"].map((x) => ListElement.fromJson(x))),
        message: json["message"],
        error: json["error"],
      );

  Map<String, dynamic> toJson() => {
        "list": List<dynamic>.from(list.map((x) => x.toJson())),
        "message": message,
        "error": error,
      };
}

class ListElement {
  ListElement({
    this.suratMasukId,
    this.tahun,
    this.nomorSurat,
    this.tglSurat,
    this.tglDiteruskan,
    this.tglBatas,
    this.jenis,
    this.jenisTujuan,
    this.kepada,
    this.perihal,
    this.klasifikasiId,
    this.instansiAsal,
    this.alamatAsal,
    this.kotaAsal,
    this.keteranganAsal,
    this.satkerTujuanId,
    this.isiRingkas,
    this.jumlahLampiran,
    this.catatan,
    this.terbalas,
    this.terdisposisi,
    this.satkerEntryId,
    this.tglEntry,
    this.userLoginId,
    this.namaUser,
    this.tglUpdate,
    this.noUrutSurat,
    this.noUrutSurat2,
    this.tembusan,
    this.isRadiogram,
    this.satkerAsalId,
    this.createdAt,
    this.satkerAsal,
    this.noUrut,
    this.terbaca,
    this.satkerTujuan,
    this.klasifikasi,
    this.type,
    this.typeSurat,
  });

  String suratMasukId;
  String tahun;
  String nomorSurat;
  DateTime tglSurat;
  DateTime tglDiteruskan;
  dynamic tglBatas;
  Jenis jenis;
  JenisTujuan jenisTujuan;
  Kepada kepada;
  String perihal;
  String klasifikasiId;
  String instansiAsal;
  String alamatAsal;
  KotaAsal kotaAsal;
  dynamic keteranganAsal;
  String satkerTujuanId;
  dynamic isiRingkas;
  dynamic jumlahLampiran;
  dynamic catatan;
  String terbalas;
  String terdisposisi;
  SatkerEntryId satkerEntryId;
  DateTime tglEntry;
  String userLoginId;
  NamaUser namaUser;
  DateTime tglUpdate;
  String noUrutSurat;
  String noUrutSurat2;
  String tembusan;
  dynamic isRadiogram;
  String satkerAsalId;
  DateTime createdAt;
  dynamic satkerAsal;
  String noUrut;
  bool terbaca;
  SatkerTujuan satkerTujuan;
  String klasifikasi;
  String type;
  TypeSurat typeSurat;

  factory ListElement.fromJson(Map<String, dynamic> json) => ListElement(
        suratMasukId: json["SuratMasukID"],
        tahun: json["Tahun"],
        nomorSurat: json["NomorSurat"],
        tglSurat:
            json["TglSurat"] == null ? null : DateTime.parse(json["TglSurat"]),
        tglDiteruskan: DateTime.parse(json["TglDiteruskan"]),
        tglBatas: json["TglBatas"],
        jenis: jenisValues.map[json["Jenis"]],
        jenisTujuan: jenisTujuanValues.map[json["JenisTujuan"]],
        kepada: kepadaValues.map[json["Kepada"]],
        perihal: json["Perihal"],
        klasifikasiId: json["KlasifikasiID"],
        instansiAsal: json["InstansiAsal"],
        alamatAsal: json["AlamatAsal"] == null ? null : json["AlamatAsal"],
        kotaAsal: json["KotaAsal"] == null
            ? null
            : kotaAsalValues.map[json["KotaAsal"]],
        keteranganAsal: json["KeteranganAsal"],
        satkerTujuanId: json["SatkerTujuanID"],
        isiRingkas: json["IsiRingkas"],
        jumlahLampiran: json["JumlahLampiran"],
        catatan: json["Catatan"],
        terbalas: json["Terbalas"],
        terdisposisi: json["Terdisposisi"],
        satkerEntryId: satkerEntryIdValues.map[json["SatkerEntryID"]],
        tglEntry: DateTime.parse(json["TglEntry"]),
        userLoginId: json["UserLoginID"],
        namaUser: namaUserValues.map[json["NamaUser"]],
        tglUpdate: json["TglUpdate"] == null
            ? null
            : DateTime.parse(json["TglUpdate"]),
        noUrutSurat: json["NoUrutSurat"],
        noUrutSurat2: json["NoUrutSurat2"],
        tembusan: json["Tembusan"],
        isRadiogram: json["isRadiogram"],
        satkerAsalId: json["SatkerAsalID"],
        createdAt: json['created_at'] == null
            ? null
            : DateTime.parse(json["created_at"] as String),
        satkerAsal: json["SatkerAsal"],
        noUrut: json["NoUrut"],
        terbaca: json["Terbaca"],
        satkerTujuan: satkerTujuanValues.map[json["SatkerTujuan"]],
        klasifikasi: json["Klasifikasi"],
        type: json["Type"],
        typeSurat: typeSuratValues.map[json["TypeSurat"]],
      );

  Map<String, dynamic> toJson() => {
        "SuratMasukID": suratMasukId,
        "Tahun": tahun,
        "NomorSurat": nomorSurat,
        "TglSurat": tglSurat == null
            ? null
            : "${tglSurat.year.toString().padLeft(4, '0')}-${tglSurat.month.toString().padLeft(2, '0')}-${tglSurat.day.toString().padLeft(2, '0')}",
        "TglDiteruskan":
            "${tglDiteruskan.year.toString().padLeft(4, '0')}-${tglDiteruskan.month.toString().padLeft(2, '0')}-${tglDiteruskan.day.toString().padLeft(2, '0')}",
        "TglBatas": tglBatas,
        "Jenis": jenisValues.reverse[jenis],
        "JenisTujuan": jenisTujuanValues.reverse[jenisTujuan],
        "Kepada": kepadaValues.reverse[kepada],
        "Perihal": perihal,
        "KlasifikasiID": klasifikasiId,
        "InstansiAsal": instansiAsal,
        "AlamatAsal": alamatAsal == null ? null : alamatAsal,
        "KotaAsal": kotaAsal == null ? null : kotaAsalValues.reverse[kotaAsal],
        "KeteranganAsal": keteranganAsal,
        "SatkerTujuanID": satkerTujuanId,
        "IsiRingkas": isiRingkas,
        "JumlahLampiran": jumlahLampiran,
        "Catatan": catatan,
        "Terbalas": terbalas,
        "Terdisposisi": terdisposisi,
        "SatkerEntryID": satkerEntryIdValues.reverse[satkerEntryId],
        "TglEntry":
            "${tglEntry.year.toString().padLeft(4, '0')}-${tglEntry.month.toString().padLeft(2, '0')}-${tglEntry.day.toString().padLeft(2, '0')}",
        "UserLoginID": userLoginId,
        "NamaUser": namaUserValues.reverse[namaUser],
        "TglUpdate": tglUpdate == null
            ? null
            : "${tglUpdate.year.toString().padLeft(4, '0')}-${tglUpdate.month.toString().padLeft(2, '0')}-${tglUpdate.day.toString().padLeft(2, '0')}",
        "NoUrutSurat": noUrutSurat,
        "NoUrutSurat2": noUrutSurat2,
        "Tembusan": tembusan,
        "isRadiogram": isRadiogram,
        "SatkerAsalID": satkerAsalId,
        "created_at": createdAt.toIso8601String(),
        "SatkerAsal": satkerAsal,
        "NoUrut": noUrut,
        "Terbaca": terbaca,
        "SatkerTujuan": satkerTujuanValues.reverse[satkerTujuan],
        "Klasifikasi": klasifikasi,
        "Type": type,
        "TypeSurat": typeSuratValues.reverse[typeSurat],
      };
}

enum Jenis { BIASA, RAHASIA }

final jenisValues =
    EnumValues({"BIASA": Jenis.BIASA, "RAHASIA": Jenis.RAHASIA});

enum JenisTujuan { INSTANSI }

final jenisTujuanValues = EnumValues({"INSTANSI": JenisTujuan.INSTANSI});

enum Kepada { WALIKOTA, WAKIL_WALIKOTA, SEKRETARIS_DAERAH }

final kepadaValues = EnumValues({
  "Sekretaris Daerah": Kepada.SEKRETARIS_DAERAH,
  "WAKIL WALIKOTA ": Kepada.WAKIL_WALIKOTA,
  "Walikota ": Kepada.WALIKOTA
});

enum KotaAsal { PASURUAN, KOTA_ASAL_PASURUAN }

final kotaAsalValues = EnumValues(
    {"Pasuruan ": KotaAsal.KOTA_ASAL_PASURUAN, "Pasuruan": KotaAsal.PASURUAN});

enum NamaUser { ADMIN }

final namaUserValues = EnumValues({"admin": NamaUser.ADMIN});

enum SatkerEntryId { EMPTY }

final satkerEntryIdValues = EnumValues({"%": SatkerEntryId.EMPTY});

enum SatkerTujuan {
  WALIKOTA_PASURUAN,
  WAKIL_WALIKOTA_PASURUAN,
  SEKRETARIAT_DAERAH
}

final satkerTujuanValues = EnumValues({
  "SEKRETARIAT DAERAH": SatkerTujuan.SEKRETARIAT_DAERAH,
  "WAKIL WALIKOTA PASURUAN": SatkerTujuan.WAKIL_WALIKOTA_PASURUAN,
  "WALIKOTA PASURUAN": SatkerTujuan.WALIKOTA_PASURUAN
});

enum TypeSurat { SURAT_MASUK }

final typeSuratValues = EnumValues({"Surat Masuk": TypeSurat.SURAT_MASUK});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
