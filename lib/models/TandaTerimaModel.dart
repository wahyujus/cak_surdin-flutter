// To parse this JSON data, do
//
//     final tandaTerimaModel = tandaTerimaModelFromJson(jsonString);

import 'dart:convert';

TandaTerimaModel tandaTerimaModelFromJson(String str) =>
    TandaTerimaModel.fromJson(json.decode(str));

String tandaTerimaModelToJson(TandaTerimaModel data) =>
    json.encode(data.toJson());

class TandaTerimaModel {
  TandaTerimaModel({
    this.data,
    this.message,
    this.error,
  });

  Data data;
  String message;
  bool error;

  factory TandaTerimaModel.fromJson(Map<String, dynamic> json) =>
      TandaTerimaModel(
        data: Data.fromJson(json["data"]),
        message: json["message"],
        error: json["error"],
      );

  Map<String, dynamic> toJson() => {
        "data": data.toJson(),
        "message": message,
        "error": error,
      };
}

class Data {
  Data({
    this.userLoginId,
    this.namaPengirim,
    this.suratMasukId,
    this.nomorSurat,
    this.instansi,
    this.type,
    this.typeSurat,
  });

  String userLoginId;
  String namaPengirim;
  String suratMasukId;
  String nomorSurat;
  String instansi;
  String type;
  String typeSurat;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        userLoginId: json["UserLoginID"],
        namaPengirim: json["nama_pengirim"],
        suratMasukId: json["SuratMasukID"],
        nomorSurat: json["nomor_surat"],
        instansi: json["instansi"],
        type: json["type"],
        typeSurat: json["type_surat"],
      );

  Map<String, dynamic> toJson() => {
        "UserLoginID": userLoginId,
        "nama_pengirim": namaPengirim,
        "SuratMasukID": suratMasukId,
        "nomor_surat": nomorSurat,
        "instansi": instansi,
        "type": type,
        "type_surat": typeSurat,
      };
}
