// To parse this JSON data, do
//
//     final suratMasukDetailModel = suratMasukDetailModelFromJson(jsonString);

import 'dart:convert';

SuratMasukDetailModel suratMasukDetailModelFromJson(String str) =>
    SuratMasukDetailModel.fromJson(json.decode(str));

String suratMasukDetailModelToJson(SuratMasukDetailModel data) =>
    json.encode(data.toJson());

class SuratMasukDetailModel {
  SuratMasukDetailModel({
    this.list,
    this.message,
    this.error,
  });

  List<ListElement> list;
  String message;
  bool error;

  factory SuratMasukDetailModel.fromJson(Map<String, dynamic> json) =>
      SuratMasukDetailModel(
        list: List<ListElement>.from(
            json["list"].map((x) => ListElement.fromJson(x))),
        message: json["message"],
        error: json["error"],
      );

  Map<String, dynamic> toJson() => {
        "list": List<dynamic>.from(list.map((x) => x.toJson())),
        "message": message,
        "error": error,
      };
}

class ListElement {
  ListElement({
    this.id,
    this.userLoginId,
    this.namaPengirim,
    this.nomorSurat,
    this.datetime,
    this.fotoPenerima,
    this.fotoTtd,
    this.fotoSurat,
    this.createdAt,
    this.suratMasukId,
    this.instansi,
    this.type,
    this.typeSurat,
    this.namaUser,
    this.namaPenerima,
    this.tanggal,
    this.jam,
  });

  String id;
  String userLoginId;
  String namaPengirim;
  String nomorSurat;
  DateTime datetime;
  List<String> fotoPenerima;
  List<String> fotoTtd;
  List<String> fotoSurat;
  DateTime createdAt;
  String suratMasukId;
  String instansi;
  String type;
  String typeSurat;
  String namaUser;
  String namaPenerima;
  DateTime tanggal;
  String jam;

  factory ListElement.fromJson(Map<String, dynamic> json) => ListElement(
        id: json["id"],
        userLoginId: json["UserLoginID"],
        namaPengirim: json["nama_pengirim"],
        nomorSurat: json["nomor_surat"],
        datetime: DateTime.parse(json["datetime"]),
        fotoPenerima: List<String>.from(json["foto_penerima"].map((x) => x)),
        fotoTtd: List<String>.from(json["foto_ttd"].map((x) => x)),
        fotoSurat: List<String>.from(json["foto_surat"].map((x) => x)),
        createdAt: DateTime.parse(json["created_at"]),
        suratMasukId: json["SuratMasukID"],
        instansi: json["instansi"],
        type: json["type"],
        typeSurat: json["type_surat"],
        namaUser: json["nama_user"],
        namaPenerima: json["nama_penerima"],
        tanggal: DateTime.parse(json["tanggal"]),
        jam: json["jam"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "UserLoginID": userLoginId,
        "nama_pengirim": namaPengirim,
        "nomor_surat": nomorSurat,
        "datetime": datetime.toIso8601String(),
        "foto_penerima": List<dynamic>.from(fotoPenerima.map((x) => x)),
        "foto_ttd": List<dynamic>.from(fotoTtd.map((x) => x)),
        "foto_surat": List<dynamic>.from(fotoSurat.map((x) => x)),
        "created_at": createdAt.toIso8601String(),
        "SuratMasukID": suratMasukId,
        "instansi": instansi,
        "type": type,
        "type_surat": typeSurat,
        "nama_user": namaUser,
        "nama_penerima": namaPenerima,
        "tanggal":
            "${tanggal.year.toString().padLeft(4, '0')}-${tanggal.month.toString().padLeft(2, '0')}-${tanggal.day.toString().padLeft(2, '0')}",
        "jam": jam,
      };
}
