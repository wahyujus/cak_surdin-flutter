// To parse this JSON data, do
//
//     final cariSuratModel = cariSuratModelFromJson(jsonString);

import 'dart:convert';

CariSuratModel cariSuratModelFromJson(String str) =>
    CariSuratModel.fromJson(json.decode(str));

String cariSuratModelToJson(CariSuratModel data) => json.encode(data.toJson());

class CariSuratModel {
  CariSuratModel({
    this.list,
  });

  List<ListElement> list;

  factory CariSuratModel.fromJson(Map<String, dynamic> json) => CariSuratModel(
        list: List<ListElement>.from(
            json["list"].map((x) => ListElement.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "list": List<dynamic>.from(list.map((x) => x.toJson())),
      };
}

class ListElement {
  ListElement({
    this.suratMasukId,
    this.tahun,
    this.nomorSurat,
    this.tglSurat,
    this.tglDiteruskan,
    this.tglBatas,
    this.jenis,
    this.jenisTujuan,
    this.kepada,
    this.perihal,
    this.klasifikasiId,
    this.instansiAsal,
    this.alamatAsal,
    this.kotaAsal,
    this.keteranganAsal,
    this.satkerTujuanId,
    this.isiRingkas,
    this.jumlahLampiran,
    this.catatan,
    this.terbalas,
    this.terdisposisi,
    this.satkerEntryId,
    this.tglEntry,
    this.userLoginId,
    this.namaUser,
    this.tglUpdate,
    this.noUrutSurat,
    this.noUrutSurat2,
    this.tembusan,
    this.isRadiogram,
    this.satkerAsalId,
    this.createdAt,
    this.type,
    this.typeSurat,
  });

  String suratMasukId;
  String tahun;
  String nomorSurat;
  DateTime tglSurat;
  DateTime tglDiteruskan;
  dynamic tglBatas;
  String jenis;
  String jenisTujuan;
  String kepada;
  String perihal;
  String klasifikasiId;
  String instansiAsal;
  dynamic alamatAsal;
  dynamic kotaAsal;
  dynamic keteranganAsal;
  String satkerTujuanId;
  dynamic isiRingkas;
  dynamic jumlahLampiran;
  dynamic catatan;
  String terbalas;
  String terdisposisi;
  String satkerEntryId;
  DateTime tglEntry;
  String userLoginId;
  String namaUser;
  dynamic tglUpdate;
  String noUrutSurat;
  String noUrutSurat2;
  String tembusan;
  dynamic isRadiogram;
  String satkerAsalId;
  DateTime createdAt;
  String type;
  String typeSurat;

  factory ListElement.fromJson(Map<String, dynamic> json) => ListElement(
        suratMasukId: json["SuratMasukID"],
        tahun: json["Tahun"],
        nomorSurat: json["NomorSurat"],
        tglSurat: DateTime.parse(json["TglSurat"]),
        tglDiteruskan: DateTime.parse(json["TglDiteruskan"]),
        tglBatas: json["TglBatas"],
        jenis: json["Jenis"],
        jenisTujuan: json["JenisTujuan"],
        kepada: json["Kepada"],
        perihal: json["Perihal"],
        klasifikasiId: json["KlasifikasiID"],
        instansiAsal: json["InstansiAsal"],
        alamatAsal: json["AlamatAsal"],
        kotaAsal: json["KotaAsal"],
        keteranganAsal: json["KeteranganAsal"],
        satkerTujuanId: json["SatkerTujuanID"],
        isiRingkas: json["IsiRingkas"],
        jumlahLampiran: json["JumlahLampiran"],
        catatan: json["Catatan"],
        terbalas: json["Terbalas"],
        terdisposisi: json["Terdisposisi"],
        satkerEntryId: json["SatkerEntryID"],
        tglEntry: DateTime.parse(json["TglEntry"]),
        userLoginId: json["UserLoginID"],
        namaUser: json["NamaUser"],
        tglUpdate: json["TglUpdate"],
        noUrutSurat: json["NoUrutSurat"],
        noUrutSurat2: json["NoUrutSurat2"],
        tembusan: json["Tembusan"],
        isRadiogram: json["isRadiogram"],
        satkerAsalId: json["SatkerAsalID"],
        createdAt: DateTime.parse(json["created_at"]),
        type: json["Type"],
        typeSurat: json["TypeSurat"],
      );

  Map<String, dynamic> toJson() => {
        "SuratMasukID": suratMasukId,
        "Tahun": tahun,
        "NomorSurat": nomorSurat,
        "TglSurat":
            "${tglSurat.year.toString().padLeft(4, '0')}-${tglSurat.month.toString().padLeft(2, '0')}-${tglSurat.day.toString().padLeft(2, '0')}",
        "TglDiteruskan":
            "${tglDiteruskan.year.toString().padLeft(4, '0')}-${tglDiteruskan.month.toString().padLeft(2, '0')}-${tglDiteruskan.day.toString().padLeft(2, '0')}",
        "TglBatas": tglBatas,
        "Jenis": jenis,
        "JenisTujuan": jenisTujuan,
        "Kepada": kepada,
        "Perihal": perihal,
        "KlasifikasiID": klasifikasiId,
        "InstansiAsal": instansiAsal,
        "AlamatAsal": alamatAsal,
        "KotaAsal": kotaAsal,
        "KeteranganAsal": keteranganAsal,
        "SatkerTujuanID": satkerTujuanId,
        "IsiRingkas": isiRingkas,
        "JumlahLampiran": jumlahLampiran,
        "Catatan": catatan,
        "Terbalas": terbalas,
        "Terdisposisi": terdisposisi,
        "SatkerEntryID": satkerEntryId,
        "TglEntry":
            "${tglEntry.year.toString().padLeft(4, '0')}-${tglEntry.month.toString().padLeft(2, '0')}-${tglEntry.day.toString().padLeft(2, '0')}",
        "UserLoginID": userLoginId,
        "NamaUser": namaUser,
        "TglUpdate": tglUpdate,
        "NoUrutSurat": noUrutSurat,
        "NoUrutSurat2": noUrutSurat2,
        "Tembusan": tembusan,
        "isRadiogram": isRadiogram,
        "SatkerAsalID": satkerAsalId,
        "created_at": createdAt.toIso8601String(),
        "Type": type,
        "TypeSurat": typeSurat,
      };
}
