// To parse this JSON data, do
//
//     final loginModel = loginModelFromJson(jsonString);

import 'dart:convert';

LoginModel loginModelFromJson(String str) =>
    LoginModel.fromJson(json.decode(str));

String loginModelToJson(LoginModel data) => json.encode(data.toJson());

class LoginModel {
  LoginModel({
    this.message,
    this.error,
    this.data,
  });

  String message;
  bool error;
  Data data;

  factory LoginModel.fromJson(Map<String, dynamic> json) => LoginModel(
        message: json["message"],
        error: json["error"],
        data: json['data'] != null ? new Data.fromJson(json["data"]) : null,
      );

  Map<String, dynamic> toJson() => {
        "message": message,
        "error": error,
        "data": data.toJson(),
      };
}

class Data {
  Data({
    this.userLoginId,
    this.namaLogin,
    this.namaUser,
    this.satKerId,
    this.noTlp,
    this.nipUser,
    this.satker,
    this.isExternal,
    this.jenis,
    this.tu,
  });

  String userLoginId;
  String namaLogin;
  String namaUser;
  String satKerId;
  String noTlp;
  String nipUser;
  String satker;
  bool isExternal;
  String jenis;
  String tu;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        userLoginId: json["UserLoginID"],
        namaLogin: json["NamaLogin"],
        namaUser: json["NamaUser"],
        satKerId: json["SatKerID"],
        noTlp: json["no_tlp"],
        nipUser: json["NipUser"],
        satker: json["Satker"],
        isExternal: json["is_external"],
        jenis: json["Jenis"],
        tu: json["tu"],
      );

  Map<String, dynamic> toJson() => {
        "UserLoginID": userLoginId,
        "NamaLogin": namaLogin,
        "NamaUser": namaUser,
        "SatKerID": satKerId,
        "no_tlp": noTlp,
        "NipUser": nipUser,
        "Satker": satker,
        "is_external": isExternal,
        "Jenis": jenis,
        "tu": tu,
      };
}
