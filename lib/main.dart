import 'package:cak_surdin/controllers/bindings/AuthBinding.dart';
import 'package:cak_surdin/root.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await GetStorage.init();
  // await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Cak Surdin',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Root(),
      initialBinding: AuthBinding(),
      builder: EasyLoading.init(),
    );
  }
}
