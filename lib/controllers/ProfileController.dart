import 'package:cak_surdin/api/RestServices.dart';
import 'package:cak_surdin/controllers/AuthController.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:loading_animations/loading_animations.dart';

class ProfileController extends GetxController {
  var isPasswordHide = false.obs;
  var passwordController = TextEditingController().obs;
  var confirmPasswordController = TextEditingController().obs;
  final authController = Get.put(AuthController());
  GetStorage box = GetStorage();

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
  }

  Future<void> updateUserData(
    String userLoginID,
    String namaLogin,
    String namaUser,
    String password,
    String no_tlp,
  ) async {
    // if (ttd.path.isNotEmpty && penerima.isNotEmpty) {
    EasyLoading.show(
        status: 'loading...',
        indicator: LoadingBouncingGrid.square(
          borderSize: 3.0,
          size: 30.0,
          backgroundColor: Colors.white,
          duration: Duration(milliseconds: 1000),
        ));
    var response = await RestServices.updateProfile(
        userLoginID, namaLogin, namaUser, password, no_tlp);
    if (response != null) {
      EasyLoading.dismiss();
      authController.reLogin(box.read('username'), box.read('password'));
      Get.back();
      EasyLoading.showSuccess('$response');
    } else {
      EasyLoading.showInfo('$response');
    }
  }
  // else {
  //   EasyLoading.showInfo('user not found');
  // }
}
