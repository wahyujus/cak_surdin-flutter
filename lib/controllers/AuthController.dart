import 'package:cak_surdin/api/RestServices.dart';
import 'package:cak_surdin/models/LoginModel.dart';
import 'package:cak_surdin/models/RegisterModel.dart';
import 'package:cak_surdin/root.dart';
import 'package:cak_surdin/ui/Home.dart';
import 'package:cak_surdin/ui/auth/Login.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:loading_animations/loading_animations.dart';

class AuthController extends GetxController {
  //local
  Rx<LoginModel> loginModel = LoginModel().obs;
  var registerModel = RegisterModel().obs;
  Rx<String> userToken = ''.obs;

  //stream
  String get token => loginModel.value.data?.userLoginId;

  //storage
  GetStorage box = GetStorage();

  //helpers
  var hidePwd = true.obs;

  @override
  void onInit() {
    streamBinding();
    super.onInit();
  }

  void streamBinding() async {
    if (box.read('token') == null) {
      userToken.value = box.read('token');
      print('session 1 : ${box.read('token')} = ${userToken}');
    } else {
      await reLogin(box.read('username'), box.read('password'));
      print(
          'session saved : ${box.read('token')} = ${loginModel.value.data?.userLoginId}');
    }
  }

  Future<void> registerUser(String username, String namaLengkap,
      String password, String no_tlp, String nip) async {
    EasyLoading.show(
        status: 'loading...',
        indicator: LoadingBouncingGrid.square(
          borderSize: 3.0,
          size: 30.0,
          backgroundColor: Colors.white,
          duration: Duration(milliseconds: 1000),
        ));
    var response = await RestServices.postRegister(
        username, namaLengkap, password, no_tlp, nip);
    if (response != null) {
      EasyLoading.dismiss();
      if (response.error != true) {
        box.write('token', response.data.userLoginId);
        registerModel.value = response;
        Get.offAll(() => Root());
        EasyLoading.showSuccess(response.message);
      } else {
        EasyLoading.showInfo(response.message);
      }
    } else {
      EasyLoading.showError('$response');
    }
  }

  Future<void> reLogin(String username, String password) async {
    var response = await RestServices.postLogin(username, password);
    if (response != null) {
      EasyLoading.dismiss();
      if (response.error != true) {
        box.write('token', response.data.userLoginId);
        loginModel.value = response;
        Get.offAll(() => Home());
        EasyLoading.showSuccess(response.message);
      } else {
        EasyLoading.showInfo(response.message);
      }
    } else {
      EasyLoading.showError('$response');
    }
  }

  void postLogin(String username, String password) async {
    if (username.isNotEmpty && password.isNotEmpty) {
      EasyLoading.show(
          status: 'loading...',
          indicator: LoadingBouncingGrid.square(
            borderSize: 3.0,
            size: 30.0,
            backgroundColor: Colors.white,
            duration: Duration(milliseconds: 1000),
          ));
      box.write('username', username);
      box.write('password', password);
      reLogin(username, password);
    } else {
      Get.snackbar(
        'form tidak lengkap',
        'harap isi semua form',
        colorText: Colors.white,
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.black45,
        shouldIconPulse: true,
      );
    }
  }

  void logout() {
    userToken.value = '';
    box.remove('token');
    Get.offAll(() => Login());
  }
}
