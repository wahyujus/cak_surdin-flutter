import 'package:cak_surdin/api/RestServices.dart';
import 'package:cak_surdin/models/SuratMasukDetailModel.dart';
import 'package:cak_surdin/models/SuratMasukModel.dart';
import 'package:cak_surdin/ui/DaftarSuratMasuk/FilterSurat.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/intl.dart';
import 'package:loading_animations/loading_animations.dart';

class SuratController extends GetxController {
  //local
  Rx<SuratMasukModel> suratMasukModel = SuratMasukModel().obs;

  Rx<SuratMasukDetailModel> suratMasukDetailModel = SuratMasukDetailModel().obs;

  //stream
  SuratMasukModel get suratMasukStream => suratMasukModel.value;
  SuratMasukDetailModel get suratMasukDetailStream =>
      suratMasukDetailModel.value;

  //helpers
  final DateFormat formatter = DateFormat('d MMMM yyyy');
  final DateFormat filterFormatter = DateFormat('MMMM d yyyy');
  final DateFormat detailFormatter = DateFormat('MMMM d yyyy, hh:mm');
  GetStorage box = GetStorage();

  //sliding segmented controll
  var segmentedControlGroupValue = 0.obs;
  final Map<int, Widget> myTabs = const <int, Widget>{
    0: Text("Informasi"),
    1: Text("Disposisi")
  };

  //search filter
  var rxNomorSurat = ''.obs;
  var rxPerihal = ''.obs;
  var rxDate = ''.obs;

  @override
  void onInit() {
    streamBinding();
    super.onInit();
  }

  void streamBinding() {
    suratMasukModel.bindStream(
        Stream.fromFuture(RestServices.fetchSuratMasuk('surat_masuk')));
  }

  Future<void> fetchSuratMasuk(String jenisSurat) async {
    EasyLoading.show(
        status: 'loading...',
        indicator: LoadingBouncingGrid.square(
          borderSize: 3.0,
          size: 30.0,
          backgroundColor: Colors.white,
          duration: Duration(milliseconds: 1000),
        ));
    var response = await RestServices.fetchSuratMasuk(jenisSurat);
    EasyLoading.dismiss();
    if (response != null) {
      suratMasukModel.value = response;
    } else {
      EasyLoading.showInfo(response.message);
      print('fetchSuratMasuk gagal : $response');
    }
  }

  Future<void> fetchSuratMasukDetail(String suratMasukID, String userLoginId,
      String nomorSurat, String type) async {
    EasyLoading.show(
        status: 'loading...',
        indicator: LoadingBouncingGrid.square(
          borderSize: 3.0,
          size: 30.0,
          backgroundColor: Colors.white,
          duration: Duration(milliseconds: 1000),
        ));
    var response = await RestServices.fetchSuratMasukDetail(
        suratMasukID, userLoginId, nomorSurat, type);
    EasyLoading.dismiss();
    if (response != null) {
      suratMasukDetailModel.value = response;
    } else {
      EasyLoading.showInfo(response.message);
      print('fetchSuratMasukDetail gagal : $response');
    }
  }

  Future<void> fetchTandaTerimaPenerima(String userLoginId, String type) async {
    EasyLoading.show(
        status: 'loading...',
        indicator: LoadingBouncingGrid.square(
          borderSize: 3.0,
          size: 30.0,
          backgroundColor: Colors.white,
          duration: Duration(milliseconds: 1000),
        ));
    var response =
        await RestServices.fetchResiTandaTerimaDetail(userLoginId, type);
    EasyLoading.dismiss();
    if (response != null) {
      suratMasukDetailModel.value = response;
    } else {
      EasyLoading.showInfo(response.message);
      print('fetchSuratMasukDetail gagal : $response');
    }
  }

  //search

  void showBottomSearch(context, String jenisSurat) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      enableDrag: true,
      backgroundColor: Colors.transparent,
      builder: (context) => Padding(
        padding:
            EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
        child: Container(
            // height: MediaQuery.of(context).size.height * 0.50,
            decoration: new BoxDecoration(
              color: Colors.white,
              borderRadius: new BorderRadius.only(
                topLeft: const Radius.circular(25.0),
                topRight: const Radius.circular(25.0),
              ),
            ),
            child: FilterSurat(
              jenisSurat: jenisSurat,
            )),
      ),
    );
  }

  //fetch search
  Future<void> fetchBottomSearch(String nomorSurat, String perihal,
      String tanggal, String jenisSurat) async {
    EasyLoading.show(
        status: 'loading...',
        indicator: LoadingBouncingGrid.square(
          borderSize: 3.0,
          size: 30.0,
          backgroundColor: Colors.white,
          duration: Duration(milliseconds: 1000),
        ));
    print('bottomsearch : $nomorSurat $perihal $tanggal');
    var response = await RestServices.suratMasukFilter(
        nomorSurat, perihal, tanggal, jenisSurat);
    rxNomorSurat.value = '';
    rxPerihal.value = '';
    rxDate.value = '';
    EasyLoading.dismiss();
    if (response != null) {
      suratMasukModel.value = response;
    } else {
      EasyLoading.showInfo(response.message);
      print('fetchSuratMasuk gagal : $response');
    }
  }
}
