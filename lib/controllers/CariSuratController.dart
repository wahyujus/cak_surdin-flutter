import 'package:cak_surdin/api/RestServices.dart';
import 'package:cak_surdin/models/CariSuratModel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/intl.dart';
import 'package:loading_animations/loading_animations.dart';

class CariSuratController extends GetxController {
  //local
  Rx<CariSuratModel> cariSuratModel = CariSuratModel().obs;

  //stream
  CariSuratModel get cariSuratModelStream => cariSuratModel.value;

  //helpers
  final DateFormat formatter = DateFormat('d MMMM yyyy HH:mm');
  GetStorage box = GetStorage();

  //sliding segmented controll
  var segmentedControlGroupValue = 0.obs;
  final Map<int, Widget> myTabs = const <int, Widget>{
    0: Text("Informasi"),
    1: Text("Disposisi")
  };

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
  }

  void streamBinding() {
    // cariSuratModel.bindStream(
    //     Stream.fromFuture(RestServices.fetchCariSurat('nomerSurat')));
  }

  Future<void> fetchCariSurat(String nomerSurat) async {
    EasyLoading.show(
        status: 'loading...',
        indicator: LoadingBouncingGrid.square(
          borderSize: 3.0,
          size: 30.0,
          backgroundColor: Colors.white,
          duration: Duration(milliseconds: 1000),
        ));
    var response = await RestServices.fetchCariSurat(nomerSurat);
    EasyLoading.dismiss();
    if (response != null) {
      cariSuratModel.value = response;
    } else {
      EasyLoading.dismiss();
      EasyLoading.showInfo(response.list.toString());
    }
  }
}
