import 'dart:io';

import 'package:cak_surdin/api/RestServices.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:loading_animations/loading_animations.dart';

import 'SuratController.dart';

class CreateResiController extends GetxController {
  var asd = ''.obs;
  var signature = File('').obs;
  var isLoading = false.obs;
  //local
  // Rx<>

  //menus
  var titleMenu = [
    'Foto Penerima',
    'Tanda Tangan Penerima',
  ];

  final suratController = Get.put(SuratController());

  List<Icon> iconMenu = [
    Icon(
      Icons.image,
      color: Colors.red,
    ),
    Icon(
      CupertinoIcons.signature,
      color: Colors.blue,
    ),
  ];
  //menus-->

  Future<void> postResi(
    String userLoginID,
    String namaPengirim,
    String suratMasukID,
    String nomorSurat,
    String instansi,
    String type,
    String typeSurat,
    List<String> penerima,
    List<String> surat,
    File ttd,
  ) async {
    isLoading.value = true;

    if (ttd.path.isNotEmpty && penerima != null) {
      EasyLoading.show(
          status: 'loading...',
          indicator: LoadingBouncingGrid.square(
            borderSize: 3.0,
            size: 30.0,
            backgroundColor: Colors.white,
            duration: Duration(milliseconds: 1000),
          ));
      var response = await RestServices.postResi(
          userLoginID,
          namaPengirim,
          suratMasukID,
          nomorSurat,
          instansi,
          type,
          typeSurat,
          penerima,
          surat,
          ttd);
      if (response != null) {
        EasyLoading.dismiss();
        suratController.fetchSuratMasukDetail(
            suratMasukID, userLoginID, nomorSurat, '');
        Get.back();
        EasyLoading.showSuccess(response);
        signature.value = File('');
      } else {
        EasyLoading.showInfo(response);
        signature.value = File('');
      }
    } else {
      print('ddddd');
      signature.value = File('');
      EasyLoading.showInfo('image not found');
    }
  }

  Future<void> postResiPengantar(
    String userLoginID,
    String namaPengirim,
    String nomorSurat,
    String type,
    String instansi,
    List<String> penerima,
    List<String> surat,
    File ttd,
  ) async {
    isLoading.value = true;
    if (ttd.path.isNotEmpty && penerima != null) {
      EasyLoading.show(
          status: 'loading...',
          indicator: LoadingBouncingGrid.square(
            borderSize: 3.0,
            size: 30.0,
            backgroundColor: Colors.white,
            duration: Duration(milliseconds: 1000),
          ));
      var response = await RestServices.resiTandaTerima(
        userLoginID,
        namaPengirim,
        nomorSurat,
        type,
        instansi,
        penerima,
        surat,
        ttd,
      );
      if (response != null) {
        EasyLoading.dismiss();
        suratController.fetchTandaTerimaPenerima(userLoginID, type);
        Get.back();
        EasyLoading.showSuccess('$response');
        signature.value = File('');
      } else {
        EasyLoading.showInfo('$response');
        signature.value = File('');
      }
    } else {
      print('ddddd');
      signature.value = File('');
      EasyLoading.showInfo('image not found');
    }
  }

  // void deletePopUp()async{

  // }
}
