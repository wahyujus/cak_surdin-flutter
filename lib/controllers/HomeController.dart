import 'package:cak_surdin/api/RestServices.dart';
import 'package:cak_surdin/models/DummyModel.dart';
import 'package:cak_surdin/ui/CariSurat/CariSurat.dart';
import 'package:cak_surdin/ui/DaftarSuratMasuk/DaftarSuratMasuk.dart';
import 'package:cak_surdin/ui/Home.dart';
import 'package:cak_surdin/ui/Profile/ProfilPengguna.dart';
import 'package:cak_surdin/ui/ResiTandaTerima/ResiTandaTerima.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:line_icons/line_icons.dart';

import 'CariSuratController.dart';

class HomeController extends GetxController {
  // ***
  // local
  Rx<DummyModel> listDummy = DummyModel().obs;

  // stream
  DummyModel get listDummyStream => listDummy.value;

  //menus
  var titleMenu = [
    // 'Daftar Surat Masuk',
    // 'Resi Tanda Terima',
    'Untuk Pengirim Surat (bagian umum)',
    'Untuk Penerima Surat ',
    'Cari Surat',
    'Profile Pengguna'
  ];

  Widget inkMenus(int index) {
    var inkMenu = [
      DaftarSuratMasuk(
        title: titleMenu[index],
        type: 'pengantar',
      ),
      ResiTandaTerima(
        title: titleMenu[index],
        type: 'penerima',
      ),
      CariSurat(),
      ProfilPengguna(
        title: titleMenu[index],
        pageIndex: index,
      )
    ];
    return inkMenu[index];
  }

  List<Icon> iconMenu = [
    Icon(
      Icons.mail_outlined,
      color: Colors.blue,
      size: 30,
    ),
    Icon(
      LineIcons.receipt,
      color: Colors.green,
      size: 30,
    ),
    Icon(
      CupertinoIcons.search,
      color: Colors.deepOrange,
      size: 30,
    ),
    Icon(
      Icons.face,
      color: Colors.purple,
      size: 30,
    )
  ];
  //menus-->

  //tabBar Menus-->
  final List<Tab> homeTabs = <Tab>[
    Tab(
      text: 'Surat Masuk'.toUpperCase(),
    ),
    Tab(
      text: 'Radiogram'.toUpperCase(),
    ),
    Tab(
      text: 'Nota dinas'.toUpperCase(),
    )
  ];
  //tabBar Menus-->

  @override
  void onInit() {
    streamBinding();
    super.onInit();
  }

  void streamBinding() {
    listDummy.bindStream(Stream.fromFuture(RestServices.fetchDummy()));
  }

  void fetchDummy() async {
    var response = await RestServices.fetchDummy();
    if (response != null) {
      listDummy.value = response;
    } else {
      print('fetchDummy gagal : $response');
    }
  }
}
