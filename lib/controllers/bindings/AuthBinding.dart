import 'package:cak_surdin/controllers/AuthController.dart';
import 'package:cak_surdin/controllers/CariSuratController.dart';
import 'package:cak_surdin/controllers/HomeController.dart';
import 'package:cak_surdin/controllers/SuratController.dart';
import 'package:get/get.dart';

class AuthBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeController>(() => HomeController());
    Get.lazyPut<AuthController>(() => AuthController());
    Get.lazyPut<CariSuratController>(() => CariSuratController());
    Get.lazyPut<SuratController>(() => SuratController());
  }
}
