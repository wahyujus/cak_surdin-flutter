import 'package:cak_surdin/controllers/AuthController.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:line_icons/line_icons.dart';

class Daftar extends StatelessWidget {
  const Daftar({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final TextEditingController nipController = TextEditingController();
    final TextEditingController namaLengkapController = TextEditingController();
    final TextEditingController usernameController = TextEditingController();
    final TextEditingController passwordController = TextEditingController();
    final TextEditingController noHpController = TextEditingController();

    final authController = Get.put(AuthController());

    return Scaffold(
      appBar: AppBar(
        title: Text('Daftar'),
      ),
      body: Obx(
        () => SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 0),
            child: Column(
              children: [
                TextField(
                  controller: nipController,
                  decoration: InputDecoration(
                    prefixIcon: Icon(LineIcons.identificationCard),
                    labelText: 'NIP',
                  ),
                ),
                TextField(
                  controller: namaLengkapController,
                  decoration: InputDecoration(
                    prefixIcon: Icon(LineIcons.identificationBadge),
                    labelText: 'Nama Lengkap',
                  ),
                ),
                TextField(
                  controller: usernameController,
                  decoration: InputDecoration(
                    prefixIcon: Icon(LineIcons.userAlt),
                    labelText: 'Username',
                  ),
                ),
                TextField(
                  controller: passwordController,
                  decoration: InputDecoration(
                    prefixIcon: Icon(CupertinoIcons.padlock),
                    suffixIcon: IconButton(
                        onPressed: () {
                          authController.hidePwd.toggle();
                        },
                        icon: authController.hidePwd.value
                            ? Icon(CupertinoIcons.eye_slash_fill)
                            : Icon(CupertinoIcons.eye_fill)),
                    labelText: 'Password',
                  ),
                  obscureText: authController.hidePwd.value ? true : false,
                ),
                TextField(
                  controller: noHpController,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    prefixIcon: Icon(CupertinoIcons.phone),
                    labelText: 'No. HP',
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  width: double.maxFinite,
                  child: ElevatedButton(
                    style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(Colors.green[500])),
                    onPressed: () async {
                      if (namaLengkapController.text.isEmpty ||
                          namaLengkapController.text.isEmpty ||
                          usernameController.text.isEmpty ||
                          passwordController.text.isEmpty ||
                          noHpController.text.isEmpty) {
                        EasyLoading.showError('form belum lengkap');
                      } else {
                        await authController.registerUser(
                            usernameController.text,
                            namaLengkapController.text,
                            passwordController.text,
                            noHpController.text,
                            nipController.text);
                      }
                    },
                    child: Text('Daftar'),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 15.0),
                  child: Text('Sudah punya akun?'),
                ),
                Container(
                  width: double.maxFinite,
                  child: ElevatedButton(
                    style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(Colors.blue)),
                    onPressed: () {
                      Get.back();
                    },
                    child: Text('Masuk'),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
