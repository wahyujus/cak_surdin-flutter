import 'package:flutter/material.dart';

class Background extends StatelessWidget {
  final Widget child;

  const Background({
    Key key,
    @required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Container(
      width: double.infinity,
      height: size.height,
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Positioned(
            top: 0,
            right: 0,
            child: Icon(Icons.dangerous),
          ),
          Positioned(
            top: 0,
            left: -70,
            child: Icon(
              Icons.email,
              size: 300,
            ),
          ),
          // Positioned(
          //   top: 50,
          //   right: 30,
          //   child: Image.asset("assets/moimoi.png", width: size.width * 0.25),
          // ),
          Positioned(
            bottom: 0,
            right: 0,
            child: Icon(Icons.dangerous),
          ),
          Positioned(
            bottom: 0,
            right: 0,
            child: Icon(Icons.dangerous),
          ),
          child
        ],
      ),
    );
  }
}
