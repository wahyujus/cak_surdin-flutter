import 'package:cak_surdin/controllers/AuthController.dart';
import 'package:cak_surdin/ui/CariSurat/CariSurat.dart';
import 'package:cak_surdin/ui/auth/Daftar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Login extends GetWidget<AuthController> {
  final TextEditingController usernameController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: [
              Container(
                color: Color(0xFF2D665F),
                child: Image.asset(
                  'assets/banner_home.png',
                  fit: BoxFit.fill,
                ),
              ),
              Container(
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                  child: Column(
                    children: [
                      TextField(
                        controller: usernameController,
                        decoration: InputDecoration(
                          labelStyle: TextStyle(color: Colors.grey),
                          suffixIcon: Icon(
                            CupertinoIcons.person_crop_circle_fill,
                          ),
                          labelText: 'Username',
                          enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey)),
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.blue)),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Obx(() => TextField(
                            controller: passwordController,
                            decoration: InputDecoration(
                              labelStyle: TextStyle(color: Colors.grey),
                              suffixIcon: IconButton(
                                  icon: controller.hidePwd.value
                                      ? Icon(
                                          CupertinoIcons.eye_slash_fill,
                                          color: Colors.grey,
                                        )
                                      : Icon(
                                          CupertinoIcons.eye_fill,
                                        ),
                                  onPressed: () => controller.hidePwd.toggle()),
                              labelText: 'Password',
                              enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.grey)),
                              focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.blue)),
                            ),
                            obscureText: controller.hidePwd.value,
                          )),
                      SizedBox(height: 20),
                      MaterialButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(5),
                        ),
                        // color: Color(0xFF2D665F),
                        color: Colors.blue,
                        minWidth: MediaQuery.of(context).size.width,
                        height: 50.0,
                        child: Text(
                          'Masuk',
                          style: TextStyle(color: Colors.white, fontSize: 16),
                        ),
                        onPressed: () async {
                          print(
                              '${usernameController.text}, ${passwordController.text}');
                          controller.postLogin(
                              usernameController.text, passwordController.text);

                          //Send to API
                        },
                      ),
                      SizedBox(height: 20),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            flex: 1,
                            child: MaterialButton(
                              shape: RoundedRectangleBorder(
                                side: BorderSide(color: Colors.grey),
                                borderRadius: new BorderRadius.circular(5),
                              ),
                              // color: Color(0xFF2D665F),
                              color: Colors.white,
                              height: 50.0,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(
                                    CupertinoIcons
                                        .person_crop_circle_fill_badge_plus,
                                    color: Colors.grey,
                                  ),
                                  SizedBox(width: 5),
                                  Text(
                                    'Daftar',
                                    style: TextStyle(
                                        fontSize: 16, color: Colors.grey),
                                  ),
                                ],
                              ),
                              onPressed: () async {
                                Get.to(() => Daftar(),
                                    transition: Transition.leftToRight);
                                //Send to API
                              },
                            ),
                          ),
                          SizedBox(width: 20),
                          Expanded(
                            flex: 1,
                            child: MaterialButton(
                              shape: RoundedRectangleBorder(
                                side: BorderSide(color: Colors.grey),
                                borderRadius: new BorderRadius.circular(5),
                              ),
                              // color: Color(0xFF2D665F),
                              color: Colors.white,
                              height: 50.0,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(
                                    CupertinoIcons.search,
                                    size: 20,
                                    color: Colors.grey,
                                  ),
                                  SizedBox(width: 5),
                                  Text(
                                    'Cari Surat',
                                    style: TextStyle(
                                      fontSize: 16,
                                      color: Colors.grey,
                                    ),
                                  ),
                                ],
                              ),
                              onPressed: () async {
                                Get.to(() => CariSurat(),
                                    transition: Transition.rightToLeft);
                                //Send to API
                              },
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
