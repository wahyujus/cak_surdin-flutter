import 'package:flutter/material.dart';
import 'package:loading_animations/loading_animations.dart';

class CustomLoading extends StatelessWidget {
  const CustomLoading({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        padding: EdgeInsets.all(20),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          color: Colors.black87,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            LoadingBouncingGrid.square(
              borderSize: 3.0,
              size: 30.0,
              backgroundColor: Colors.white,
              duration: Duration(milliseconds: 1000),
            ),
            SizedBox(height: 10),
            Text(
              'Loading...',
              style: TextStyle(color: Colors.white),
            )
          ],
        ),
      ),
    );
  }
}
