import 'dart:typed_data';

import 'package:cak_surdin/ui/DaftarSuratMasuk/DetailSurat/SignaturePreview.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'dart:ui' as ui;

import 'package:signature/signature.dart';

class TandaTanganPenerima extends StatefulWidget {
  const TandaTanganPenerima({Key key}) : super(key: key);

  @override
  _TandaTanganPenerimaState createState() => _TandaTanganPenerimaState();
}

class _TandaTanganPenerimaState extends State<TandaTanganPenerima> {
  SignatureController signatureController;

  @override
  void initState() {
    signatureController = SignatureController(penColor: Colors.white);
    super.initState();
  }

  @override
  void dispose() {
    signatureController.dispose();
    super.dispose();
  }

  Future<Uint8List> exportSignature() async {
    final exportController = SignatureController(
      penColor: Colors.black,
      exportBackgroundColor: Colors.white,
      points: signatureController.points,
    );

    final signature = await exportController.toPngBytes();
    exportController.dispose();

    return signature;
  }

  Widget iconBtn(int value, IconData iconData) {
    return IconButton(
      onPressed: () async {
        if (value == 1) {
          if (signatureController.isNotEmpty) {
            final signature = await exportSignature();
            Get.to(() => SignaturePreview(signature: signature));
          } else {}
          print('check');
        } else {
          signatureController.clear();
        }
      },
      icon: Icon(
        iconData,
        color: value == 1 ? Colors.green : Colors.red,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // GlobalKey<SfSignaturePadState> _signaturePadKey = GlobalKey();

    return Scaffold(
      appBar: AppBar(
        title: Text('Digital Signature'),
      ),
      body: Column(
        children: [
          Signature(
            controller: signatureController,
            backgroundColor: Colors.black,
          ),
          Container(
            color: Colors.black,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                iconBtn(1, Icons.check),
                iconBtn(0, Icons.close),
              ],
            ),
          )
        ],
      ),
    );
  }
}
