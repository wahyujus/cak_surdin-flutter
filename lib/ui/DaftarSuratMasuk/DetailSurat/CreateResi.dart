import 'dart:io';

import 'package:cak_surdin/controllers/AuthController.dart';
import 'package:cak_surdin/controllers/CreateResiController.dart';
import 'package:cak_surdin/controllers/HomeController.dart';
import 'package:cak_surdin/controllers/SuratController.dart';
import 'package:cak_surdin/models/MultiPickerModel.dart';
import 'package:cak_surdin/ui/DaftarSuratMasuk/DetailSurat/TandaTanganPenerima.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

import 'FotoPenerima.dart';

class CreateResi extends StatefulWidget {
  const CreateResi(
      {Key key,
      this.suratMasukID,
      this.nomorSurat,
      @required this.type,
      this.instansi,
      this.typeSurat})
      : super(key: key);

  final String suratMasukID;
  final String nomorSurat;
  final String type;
  final String instansi;
  final String typeSurat;

  @override
  _CreateResiState createState() => _CreateResiState();
}

class _CreateResiState extends State<CreateResi> {
  final homeController = Get.put(HomeController());
  final authController = Get.put(AuthController());
  final createResiController = Get.put(CreateResiController());
  final suratController = Get.put(SuratController());

  final TextEditingController nomorSuratController = TextEditingController();
  final TextEditingController namaPenerimaController = TextEditingController();
  final TextEditingController instansiController = TextEditingController();

  String _fileName;
  List<PlatformFile> _paths;
  List<PlatformFile> _pathsSurat;
  File _pathsTTD;

  String _directoryPath;
  String _extension;
  bool _loadingPath = false;
  bool _multiPick = false;
  FileType _pickingType = FileType.any;
  TextEditingController _controller = TextEditingController();
  List<String> pickedPic = List<String>();
  List<String> pickedSurat = List<String>();

  @override
  void initState() {
    startup();
    super.initState();
  }

  void startup() {
    createResiController.signature.value = File('');
    createResiController.isLoading.value = false;
  }

  Future<void> pickers() async {
    _paths = (await FilePicker.platform.pickFiles(
      type: FileType.image,
      allowMultiple: true,
      // allowedExtensions: ['jpg', 'png'],
    ))
        ?.files;

    setState(() {});

    //show
    if (_paths != null) {
      var name = _paths.map((e) => e.name);
      var path = _paths.map((e) => e.path);
      print(_paths.map((e) => e.bytes));
      print(_paths.map((e) => e.size));
      print(_paths.map((e) => e.extension));

      if (path != null && name != null) {
        setState(() {
          pickedPic = path.toList();
        });
        print('ddd : $pickedPic');
      } else {}
    } else {
      // User canceled the picker
    }
  }

  void pickersSurat() async {
    _pathsSurat = (await FilePicker.platform.pickFiles(
      type: FileType.image,
      allowMultiple: true,
      // allowedExtensions: ['jpg', 'png'],
    ))
        ?.files;

    setState(() {});

    //show
    if (_pathsSurat != null) {
      var name = _pathsSurat.map((e) => e.name);
      var path = _pathsSurat.map((e) => e.path);
      print(_pathsSurat.map((e) => e.bytes));
      print(_pathsSurat.map((e) => e.size));
      print(_pathsSurat.map((e) => e.extension));

      if (path != null && name != null) {
        setState(() {
          pickedSurat = path.toList();
        });
        print('ddd : $pickedSurat');
      } else {}
    } else {
      // User canceled the picker
    }
  }

  void modalSheet(int menuType) {
    Get.bottomSheet(
      Container(
        width: double.maxFinite,
        height: MediaQuery.of(context).size.height * 0.25,
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              InkWell(
                onTap: () async {
                  if (menuType == 0) {
                    pickedPic.removeRange(0, pickedPic.length);
                    final PickedFile pickedFile = await ImagePicker()
                        .getImage(source: ImageSource.camera);

                    if (pickedFile != null) {
                      setState(() {
                        pickedPic.add(pickedFile.path);
                      });
                      print('ddd : $pickedPic');
                      Get.back();
                    }
                  } else {
                    pickedSurat.removeRange(0, pickedSurat.length);
                    final PickedFile pickedFile = await ImagePicker()
                        .getImage(source: ImageSource.camera);

                    if (pickedFile != null) {
                      setState(() {
                        pickedSurat.add(pickedFile.path);
                      });
                      print('asd : $pickedSurat');
                      Get.back();
                    }
                  }
                },
                child: Card(
                  child: ListTile(
                    leading: Icon(CupertinoIcons.camera),
                    title: Text('Camera'),
                  ),
                ),
              ),
              InkWell(
                onTap: () async {
                  if (menuType == 0) {
                    await pickers().then((value) => Get.back());
                  } else {
                    pickersSurat();
                    Get.back();
                  }
                },
                child: Card(
                  child: ListTile(
                    leading: Icon(Icons.image),
                    title: Text('Gallery'),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => createResiController.signature.value == null
          ? Center(child: CircularProgressIndicator())
          : Scaffold(
              appBar: AppBar(
                leading: CloseButton(
                  onPressed: () {
                    createResiController.signature.value = File('');
                    Get.back();
                  },
                ),
                title: Text('${widget.type.toUpperCase()} Surat (Resi)'),
                // title: Text(widget.type),

                actions: [
                  IconButton(
                      icon: Icon(Icons.send),
                      onPressed: () async {
                        if (widget.type == 'pengantar') {
                          if (createResiController.isLoading.value == false) {
                            await createResiController.postResi(
                                authController
                                    .loginModel.value.data.userLoginId,
                                namaPenerimaController.text,
                                widget.suratMasukID,
                                widget.nomorSurat,
                                instansiController.text,
                                widget.type,
                                widget.typeSurat,
                                pickedPic,
                                pickedSurat,
                                createResiController.signature.value);
                          }
                        } else {
                          if (createResiController.isLoading.value == false) {
                            await createResiController.postResiPengantar(
                              authController.loginModel.value.data.userLoginId,
                              namaPenerimaController.text,
                              nomorSuratController.text.isEmpty
                                  ? '---'
                                  : nomorSuratController.text,
                              widget.type,
                              instansiController.text,
                              pickedPic,
                              pickedSurat,
                              createResiController.signature.value,
                            );
                            // print(widget.type);
                          }
                        }
                      })
                ],
              ),
              body: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 15.0, vertical: 15.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      widget.type == 'penerima'
                          ? Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Nomor Surat',
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold)),
                                SizedBox(height: 5),
                                CupertinoTextField(
                                  padding: EdgeInsets.all(10),
                                  placeholder: 'Nomor Surat',
                                  clearButtonMode:
                                      OverlayVisibilityMode.editing,
                                  controller: nomorSuratController,
                                ),
                              ],
                            )
                          : Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Nomor Surat',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                SizedBox(height: 5),
                                Text(widget.nomorSurat ?? ''),
                              ],
                            ),
                      SizedBox(height: 20),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                              widget.type == 'penerima'
                                  ? 'Nama Pengantar'
                                  : 'Nama Penerima',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          SizedBox(height: 5),
                          CupertinoTextField(
                            padding: EdgeInsets.all(10),
                            placeholder: widget.type == 'penerima'
                                ? 'Nama Pengantar'
                                : 'Nama Penerima',
                            clearButtonMode: OverlayVisibilityMode.editing,
                            controller: namaPenerimaController,
                          ),
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 20),
                          Text('Instansi',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          SizedBox(height: 5),
                          CupertinoTextField(
                            padding: EdgeInsets.all(10),
                            placeholder: 'Instansi',
                            clearButtonMode: OverlayVisibilityMode.editing,
                            controller: instansiController,
                          ),
                        ],
                      ),
                      SizedBox(height: 20),
                      Column(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('Attachment',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          SizedBox(height: 5),
                          StaggeredGridView.count(
                            mainAxisSpacing: 10,
                            crossAxisSpacing: 10,
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            crossAxisCount: 2,
                            children: [
                              InkWell(
                                onTap: () {
                                  modalSheet(0);
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(10),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.5),
                                        spreadRadius: 2,
                                        blurRadius: 3,
                                        offset: Offset(
                                            0, 1), // changes position of shadow
                                      ),
                                    ],
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Icon(
                                          Icons.image,
                                          color: Colors.red,
                                        ),
                                        SizedBox(height: 5),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              widget.type == 'penerima'
                                                  ? 'Foto Pengantar'
                                                  : 'Foto Penerima',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w500),
                                            ),
                                            pickedPic != null
                                                ? Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      Text(
                                                          '${pickedPic.length} foto telah dipilih'),
                                                      pickedPic.length == 0
                                                          ? Icon(
                                                              Icons.cancel,
                                                              color: Colors.red,
                                                            )
                                                          : Icon(
                                                              Icons
                                                                  .check_circle,
                                                              color:
                                                                  Colors.green,
                                                            )
                                                    ],
                                                  )
                                                : SizedBox.shrink()
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              InkWell(
                                onTap: () => Get.to(() => TandaTanganPenerima(),
                                    transition: Transition.rightToLeft),
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(10),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.5),
                                        spreadRadius: 2,
                                        blurRadius: 3,
                                        offset: Offset(
                                            0, 1), // changes position of shadow
                                      ),
                                    ],
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Icon(
                                          CupertinoIcons.signature,
                                          color: Colors.blue,
                                        ),
                                        SizedBox(height: 5),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              widget.type == 'penerima'
                                                  ? 'Tanda Tangan Pengantar'
                                                  : 'Tanda Tangan Penerima',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w500),
                                            ),
                                            createResiController.signature.value
                                                    .path.isNotEmpty
                                                ? Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      Text('Added'),
                                                      Icon(
                                                        CupertinoIcons
                                                            .checkmark_circle_fill,
                                                        color: Colors.green,
                                                      ),
                                                    ],
                                                  )
                                                : SizedBox.shrink()
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              widget.type == 'penerima'
                                  ? InkWell(
                                      onTap: () {
                                        // pickersSurat();
                                        modalSheet(1);
                                      },
                                      child: Container(
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          boxShadow: [
                                            BoxShadow(
                                              color:
                                                  Colors.grey.withOpacity(0.5),
                                              spreadRadius: 2,
                                              blurRadius: 3,
                                              offset: Offset(0,
                                                  1), // changes position of shadow
                                            ),
                                          ],
                                        ),
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Icon(
                                                Icons.mail,
                                                color: Colors.green,
                                              ),
                                              SizedBox(height: 5),
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    'Foto Surat',
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w500),
                                                  ),
                                                  pickedSurat != null
                                                      ? Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceBetween,
                                                          children: [
                                                            Text(
                                                                '${pickedSurat.length} foto telah dipilih'),
                                                            pickedSurat.length ==
                                                                    0
                                                                ? Icon(
                                                                    Icons
                                                                        .cancel,
                                                                    color: Colors
                                                                        .red,
                                                                  )
                                                                : Icon(
                                                                    Icons
                                                                        .check_circle,
                                                                    color: Colors
                                                                        .green,
                                                                  )
                                                          ],
                                                        )
                                                      : SizedBox.shrink()
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    )
                                  : SizedBox.shrink(),
                            ],
                            staggeredTiles: [
                              StaggeredTile.fit(2),
                              StaggeredTile.fit(2),
                              StaggeredTile.fit(2),
                            ],
                          ),
                          SizedBox(height: 10),
                          pickedPic == null || pickedPic.length == 0
                              ? SizedBox.shrink()
                              : Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      widget.type == 'pengantar'
                                          ? 'Foto Pengantar'
                                          : 'Foto Penerima',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Container(
                                      height: 150,
                                      child: ListView.separated(
                                        scrollDirection: Axis.horizontal,
                                        shrinkWrap: true,
                                        itemCount: pickedPic != null &&
                                                pickedPic.isNotEmpty
                                            ? pickedPic.length
                                            : 0,
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          return pickedPic.isEmpty ||
                                                  pickedPic == null
                                              ? SizedBox.shrink()
                                              : Padding(
                                                  padding:
                                                      const EdgeInsets.all(8.0),
                                                  child: Stack(
                                                    children: [
                                                      Container(
                                                        decoration:
                                                            BoxDecoration(
                                                          color: Colors.white,
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(10),
                                                          boxShadow: [
                                                            BoxShadow(
                                                              color: Colors.grey
                                                                  .withOpacity(
                                                                      0.5),
                                                              spreadRadius: 2,
                                                              blurRadius: 3,
                                                              offset: Offset(0,
                                                                  1), // changes position of shadow
                                                            ),
                                                          ],
                                                        ),
                                                        child: ClipRRect(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      8.0),
                                                          child: Image.file(
                                                            File(pickedPic
                                                                .map((e) => e)
                                                                .toList()[index]),
                                                            fit: BoxFit
                                                                .fitHeight,
                                                          ),
                                                        ),
                                                      ),
                                                      Positioned(
                                                        top: 5,
                                                        right: 5,
                                                        child: CircleAvatar(
                                                          maxRadius: 10,
                                                          child: InkWell(
                                                            onTap: () {
                                                              setState(() {
                                                                pickedPic
                                                                    .removeAt(
                                                                        index);
                                                              });
                                                            },
                                                            child: Icon(
                                                              Icons.close,
                                                              size: 15,
                                                              color:
                                                                  Colors.white,
                                                            ),
                                                          ),
                                                          backgroundColor:
                                                              Colors.red,
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                );
                                        },
                                        separatorBuilder:
                                            (BuildContext context, int index) =>
                                                const Divider(),
                                      ),
                                    ),
                                  ],
                                ),
                          createResiController.signature.value.path.isEmpty
                              ? SizedBox.shrink()
                              : Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Foto Tanda Tangan Penerima',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Container(
                                        height: 150,
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Stack(
                                            children: [
                                              Container(
                                                decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                  boxShadow: [
                                                    BoxShadow(
                                                      color: Colors.grey
                                                          .withOpacity(0.5),
                                                      spreadRadius: 2,
                                                      blurRadius: 3,
                                                      offset: Offset(0,
                                                          1), // changes position of shadow
                                                    ),
                                                  ],
                                                ),
                                                child: ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          8.0),
                                                  child: Image.file(
                                                    File(createResiController
                                                        .signature.value.path),
                                                    fit: BoxFit.fitHeight,
                                                  ),
                                                ),
                                              ),
                                              Positioned(
                                                top: 5,
                                                right: 5,
                                                child: CircleAvatar(
                                                  maxRadius: 10,
                                                  child: InkWell(
                                                    onTap: () {
                                                      createResiController
                                                          .signature
                                                          .value = File('');
                                                    },
                                                    child: Icon(
                                                      Icons.close,
                                                      size: 15,
                                                      color: Colors.white,
                                                    ),
                                                  ),
                                                  backgroundColor: Colors.red,
                                                ),
                                              )
                                            ],
                                          ),
                                        )),
                                  ],
                                ),
                          pickedSurat == null || pickedSurat.length == 0
                              ? SizedBox.shrink()
                              : Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Foto Surat',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Container(
                                      height: 150,
                                      child: ListView.separated(
                                        scrollDirection: Axis.horizontal,
                                        shrinkWrap: true,
                                        itemCount: pickedSurat != null &&
                                                pickedSurat.isNotEmpty
                                            ? pickedSurat.length
                                            : 0,
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          return pickedSurat.isEmpty ||
                                                  pickedSurat == null
                                              ? SizedBox.shrink()
                                              : Padding(
                                                  padding:
                                                      const EdgeInsets.all(8.0),
                                                  child: Stack(
                                                    children: [
                                                      Container(
                                                        decoration:
                                                            BoxDecoration(
                                                          color: Colors.white,
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(10),
                                                          boxShadow: [
                                                            BoxShadow(
                                                              color: Colors.grey
                                                                  .withOpacity(
                                                                      0.5),
                                                              spreadRadius: 2,
                                                              blurRadius: 3,
                                                              offset: Offset(0,
                                                                  1), // changes position of shadow
                                                            ),
                                                          ],
                                                        ),
                                                        child: ClipRRect(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      8.0),
                                                          child: Image.file(
                                                            File(pickedSurat
                                                                .map((e) => e)
                                                                .toList()[index]),
                                                            fit: BoxFit
                                                                .fitHeight,
                                                          ),
                                                        ),
                                                      ),
                                                      Positioned(
                                                        top: 5,
                                                        right: 5,
                                                        child: CircleAvatar(
                                                          maxRadius: 10,
                                                          child: InkWell(
                                                            onTap: () {
                                                              setState(() {
                                                                pickedSurat
                                                                    .removeAt(
                                                                        index);
                                                              });
                                                            },
                                                            child: Icon(
                                                              Icons.close,
                                                              size: 15,
                                                              color:
                                                                  Colors.white,
                                                            ),
                                                          ),
                                                          backgroundColor:
                                                              Colors.red,
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                );
                                        },
                                        separatorBuilder:
                                            (BuildContext context, int index) =>
                                                const Divider(),
                                      ),
                                    ),
                                  ],
                                ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
    );
  }
}
