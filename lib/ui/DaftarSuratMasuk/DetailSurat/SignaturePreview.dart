import 'dart:io';
import 'dart:typed_data';

import 'package:cak_surdin/controllers/CreateResiController.dart';
import 'package:file_saver/file_saver.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:open_file/open_file.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:path/path.dart' as path;

class SignaturePreview extends StatelessWidget {
  final Uint8List signature;

  const SignaturePreview({Key key, @required this.signature}) : super(key: key);
  // const SignaturePreview({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final createResiController = Get.put(CreateResiController());
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        leading: CloseButton(),
        title: Text('preview'),
        actions: [
          IconButton(
            onPressed: () {
              storeSignature(createResiController, signature);
            },
            icon: Icon(Icons.check),
          ),
        ],
      ),
      body: Center(
        child: Image.memory(
          signature,
          width: double.infinity,
        ),
      ),
    );
  }

  Future storeSignature(
      CreateResiController createResiController, Uint8List signature) async {
    //store image to local and open_file
    final status = await Permission.storage.status;
    if (!status.isGranted) {
      await Permission.storage.request();
    } else if (status.isDenied) {
      openAppSettings();
      EasyLoading.showInfo('storage not granted');
    }
    final DateFormat formatter = DateFormat('dMMMMyyyyHHmm');

    final time = formatter.format(DateTime.now());
    final name = 'signature_$time';

    final result = await FileSaver.instance
        .saveFile(name, signature, 'png', mimeType: MimeType.PNG);

    createResiController.signature.value = File(result);
    EasyLoading.showSuccess('Signature added');
    Get.back();
    Get.back();

    // final result =
    //     await ImageGallerySaver.saveImage(signature, quality: 60, name: name);

    // if (result['isSuccess'] == true) {
    //   createResiController.signature.value =
    //       File(result['filePath'].toString().replaceAll('file://', ''));
    //   EasyLoading.showSuccess('Signature added');
    //   Get.back();
    //   Get.back();
    // }

    // final isSuccess = result['filePath'].toString().replaceAll('file://', '');
    print(result);
    print(createResiController.signature.value);

    // OpenFile.open(isSuccess);
  }
}
