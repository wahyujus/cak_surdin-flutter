import 'package:cak_surdin/controllers/AuthController.dart';
import 'package:cak_surdin/controllers/HomeController.dart';
import 'package:cak_surdin/controllers/SuratController.dart';
import 'package:cak_surdin/ui/DaftarSuratMasuk/DetailSurat/DetailTandaTerima.dart';
import 'package:clipboard/clipboard.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:line_icons/line_icon.dart';
import 'package:line_icons/line_icons.dart';

import 'CreateResi.dart';

class DetailSuratMasuk extends StatefulWidget {
  const DetailSuratMasuk({
    Key key,
    @required this.nomorSurat,
    @required this.tanggalSurat,
    @required this.perihal,
    @required this.type,
    @required this.suratMasukID,
    @required this.instansi,
    @required this.typeSurat,
  }) : super(key: key);

  final String suratMasukID;
  final String nomorSurat;
  final String tanggalSurat;
  final String perihal;
  final String type;
  final String instansi;
  final String typeSurat;

  @override
  _DetailSuratMasukState createState() => _DetailSuratMasukState();
}

class _DetailSuratMasukState extends State<DetailSuratMasuk> {
  final suratController = Get.put(SuratController());
  final authController = Get.put(AuthController());

  @override
  void initState() {
    startUp();
    super.initState();
  }

  void startUp() async {
    await suratController.fetchSuratMasukDetail(
        widget.suratMasukID,
        authController.loginModel.value.data.userLoginId,
        widget.nomorSurat,
        '');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Tanda Terima Surat'),
        actions: [
          IconButton(
              icon: Icon(LineIcons.edit),
              onPressed: () {
                Get.to(() => CreateResi(
                      type: widget.type,
                      nomorSurat: widget.nomorSurat,
                      suratMasukID: widget.suratMasukID,
                      instansi: widget.instansi,
                      typeSurat: widget.typeSurat,
                    ));
              }),
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Card(
                color: Colors.blue[100],
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10.0),
                  child: ListTile(
                    title: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Nomor Surat :',
                          style: TextStyle(
                              fontSize: 14, fontWeight: FontWeight.bold),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              widget.nomorSurat,
                              style: TextStyle(fontSize: 14),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            InkWell(
                              onTap: () {
                                FlutterClipboard.copy(widget.nomorSurat ?? '')
                                    .then((value) => Get.snackbar(
                                        'Nomor Surat di-copy',
                                        '${widget.nomorSurat} berhasil di copy',
                                        snackPosition: SnackPosition.BOTTOM,
                                        backgroundColor: Colors.green,
                                        colorText: Colors.white));
                              },
                              child: Icon(
                                Icons.copy,
                                size: 16,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 10),
                        Text(
                          'Tanggal Surat :',
                          style: TextStyle(
                              fontSize: 14, fontWeight: FontWeight.bold),
                        ),
                        Text(
                          widget.tanggalSurat,
                          style: TextStyle(fontSize: 14),
                        ),
                        SizedBox(height: 10),
                        Text(
                          'Perihal :',
                          style: TextStyle(
                              fontSize: 14, fontWeight: FontWeight.bold),
                        ),
                        Text(
                          widget.perihal,
                          style: TextStyle(fontSize: 14),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 10.0, horizontal: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    PopupMenuButton(
                      child: Row(
                        children: [
                          Text(
                            'FILTER',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          SizedBox(width: 5),
                          Icon(CupertinoIcons.sort_down),
                        ],
                      ),
                      itemBuilder: (value) => <PopupMenuItem<String>>[
                        new PopupMenuItem<String>(
                            textStyle:
                                TextStyle(fontSize: 12, color: Colors.black),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text('Semua'),
                                Icon(
                                  LineIcons.mailBulk,
                                  color: Colors.blue,
                                )
                              ],
                            ),
                            value: 'Semua'),
                        new PopupMenuItem<String>(
                            textStyle:
                                TextStyle(fontSize: 12, color: Colors.black),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text('Penerima '),
                                Icon(
                                  Icons.mark_email_unread_outlined,
                                  color: Colors.blue,
                                )
                              ],
                            ),
                            value: 'Penerima'),
                        new PopupMenuItem<String>(
                            textStyle:
                                TextStyle(fontSize: 12, color: Colors.black),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text('Pengantar '),
                                Icon(
                                  Icons.contact_mail_outlined,
                                  color: Colors.blue,
                                )
                              ],
                            ),
                            value: 'Pengantar'),
                      ],
                      onSelected: (String value) {
                        print('object $value');
                        switch (value) {
                          case 'Penerima':
                            suratController.fetchSuratMasukDetail(
                                widget.suratMasukID,
                                authController
                                    .loginModel.value.data.userLoginId,
                                widget.nomorSurat,
                                value.toLowerCase());
                            break;
                          case 'Pengantar':
                            suratController.fetchSuratMasukDetail(
                                widget.suratMasukID,
                                authController
                                    .loginModel.value.data.userLoginId,
                                widget.nomorSurat,
                                value.toLowerCase());
                            break;
                          case 'Semua':
                            suratController.fetchSuratMasukDetail(
                                widget.suratMasukID,
                                authController
                                    .loginModel.value.data.userLoginId,
                                widget.nomorSurat,
                                '');
                            break;
                          default:
                            suratController.fetchSuratMasukDetail(
                                widget.suratMasukID,
                                authController
                                    .loginModel.value.data.userLoginId,
                                widget.nomorSurat,
                                '');
                        }
                      },
                    )
                  ],
                ),
              ),
              GetX<SuratController>(
                init: suratController,
                builder: (SuratController controller) {
                  if (controller.suratMasukDetailModel.value.list != null &&
                      controller.suratMasukDetailModel.value.list.isNotEmpty) {
                    return RefreshIndicator(
                      onRefresh: () async =>
                          await suratController.fetchSuratMasuk(widget.type),
                      child: ListView.builder(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount:
                            controller.suratMasukDetailModel.value.list.length,
                        itemBuilder: (BuildContext context, int index) {
                          var listItem = controller
                              .suratMasukDetailModel.value.list[index];
                          return Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 5, horizontal: 5),
                            child: InkWell(
                              onTap: () {
                                Get.to(() => DetailTandaTerima(
                                      instansi: listItem.instansi,
                                      jenisSurat: listItem.type,
                                      namaPenerima: listItem.namaPenerima,
                                      namaPengirim: listItem.namaPengirim,
                                      nomorSurat: listItem.nomorSurat,
                                      tanggalSurat: controller.detailFormatter
                                          .format(listItem.createdAt)
                                          .toString(),
                                      id: listItem.id,
                                      suratMasukID: listItem.suratMasukId,
                                      penerimaSurat: listItem.fotoPenerima,
                                      ttd: listItem.fotoTtd,
                                      typeSurat: widget.typeSurat,
                                      fotoSurat: listItem.fotoSurat,
                                    ));
                              },
                              child: Column(
                                children: [
                                  Container(
                                    width: double.infinity,
                                    decoration: BoxDecoration(
                                      color: listItem.type.toLowerCase() !=
                                              'penerima'
                                          ? Colors.blue
                                          : Colors.green,
                                      borderRadius: BorderRadius.vertical(
                                        top: Radius.circular(10),
                                        // bottom: Radius.circular(10),
                                      ),
                                      boxShadow: [
                                        BoxShadow(
                                            color: Colors.grey[500],
                                            blurRadius: 4,
                                            offset: Offset(2, 0))
                                      ],
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          left: 10.0,
                                          right: 10.0,
                                          top: 7.0,
                                          bottom: 7.0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            listItem.type.toUpperCase() +
                                                ' SURAT',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Row(
                                            children: [
                                              Text(
                                                'Lihat Foto Bukti',
                                                style: TextStyle(
                                                    color: Colors.white),
                                              ),
                                              // Icon(
                                              //   Icons.image,
                                              //   color: Colors.white,
                                              // ),
                                              Icon(
                                                Icons.chevron_right,
                                                color: Colors.white,
                                              ),
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                  Container(
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.vertical(
                                        // top: Radius.circular(10),
                                        bottom: Radius.circular(10),
                                      ),
                                      // gradient: LinearGradient(
                                      //     colors: listItem.nomorSurat == '---'
                                      //         ? [Colors.orange, Colors.white]
                                      //         : [Colors.blue, Colors.white],
                                      //     begin: Alignment(-0.95, 0),
                                      //     end: Alignment(-0.949, 0)),
                                      boxShadow: [
                                        BoxShadow(
                                            color: Colors.grey[500],
                                            blurRadius: 4,
                                            offset: Offset(0, 2))
                                      ],
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          left: 10.0,
                                          right: 10.0,
                                          top: 5.0,
                                          bottom: 5.0),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text(listItem.nomorSurat ?? '',
                                                  style:
                                                      TextStyle(fontSize: 14)),
                                              Text(
                                                  controller.detailFormatter
                                                      .format(
                                                          listItem.createdAt)
                                                      .toString(),
                                                  style:
                                                      TextStyle(fontSize: 14)),
                                            ],
                                          ),
                                          SizedBox(height: 10),
                                          Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text('Nama Penerima'),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: [
                                                  Icon(Icons.forward_to_inbox),
                                                  SizedBox(width: 5),
                                                  Expanded(
                                                    child: Text(
                                                      listItem.namaPengirim ??
                                                          '',
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontWeight:
                                                              FontWeight.w600),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                          SizedBox(height: 10),
                                          Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text('Nama Pengirim'),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: [
                                                  Icon(Icons
                                                      .mark_email_read_outlined),
                                                  SizedBox(width: 5),
                                                  Expanded(
                                                    child: Text(
                                                      listItem.namaPenerima ??
                                                          '',
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontWeight:
                                                              FontWeight.w600),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                          SizedBox(height: 10),
                                          Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text('Instansi'),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: [
                                                  Icon(Icons.blur_on),
                                                  SizedBox(width: 5),
                                                  Expanded(
                                                    child: Text(
                                                      listItem.instansi ?? '',
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontWeight:
                                                              FontWeight.w600),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    );
                  } else {
                    return Center(
                        child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Icon(
                          LineIcons.mailBulk,
                          size: 100,
                          color: Colors.grey,
                        ),
                        Text(
                          'Data Kosong',
                          style: TextStyle(color: Colors.grey),
                        )
                      ],
                    ));
                  }
                },
              )
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Get.to(() => CreateResi(
                type: widget.type,
                nomorSurat: widget.nomorSurat,
                suratMasukID: widget.suratMasukID,
                instansi: widget.instansi,
                typeSurat: widget.typeSurat,
              ));
        },
        child: Icon(LineIcons.edit),
      ),
    );
  }
}
