import 'package:cak_surdin/api/RestServices.dart';
import 'package:cak_surdin/controllers/AuthController.dart';
import 'package:cak_surdin/controllers/SuratController.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:full_screen_image/full_screen_image.dart';
import 'package:get/get.dart';

class DetailTandaTerima extends StatelessWidget {
  const DetailTandaTerima(
      {Key key,
      this.jenisSurat,
      @required this.nomorSurat,
      @required this.tanggalSurat,
      @required this.namaPengirim,
      @required this.namaPenerima,
      @required this.instansi,
      @required this.id,
      @required this.suratMasukID,
      @required this.penerimaSurat,
      @required this.ttd,
      this.typeSurat,
      @required this.fotoSurat})
      : super(key: key);

  final String id;
  final String jenisSurat;
  final String suratMasukID;
  final String nomorSurat;
  final String tanggalSurat;
  final String namaPengirim;
  final String namaPenerima;
  final String instansi;
  final dynamic penerimaSurat;
  final dynamic ttd;
  final dynamic fotoSurat;
  final String typeSurat;

  @override
  Widget build(BuildContext context) {
    final suratController = Get.put(SuratController());
    final authController = Get.put(AuthController());
    return Scaffold(
        appBar: AppBar(
          title: Text(nomorSurat ?? '-'),
          actions: [
            IconButton(
                icon: Icon(Icons.delete_forever),
                onPressed: () {
                  showCupertinoModalPopup(
                    context: context,
                    builder: (BuildContext context) {
                      return CupertinoActionSheet(
                        title: Text(
                            'Apakah anda yakin untuk menghapus $namaPenerima?'),
                        actions: [
                          CupertinoActionSheetAction(
                            child: const Text(
                              'Hapus',
                              style: TextStyle(color: Colors.red),
                            ),
                            onPressed: () async {
                              await RestServices.deleteResi(id);
                              Get.back();
                              Get.back();
                              await suratController.fetchSuratMasukDetail(
                                  suratMasukID,
                                  authController
                                      .loginModel.value.data.userLoginId,
                                  nomorSurat,
                                  typeSurat);
                              Get.snackbar('$namaPenerima Deleted',
                                  'Resi $nomorSurat Berhasil di hapus',
                                  colorText: Colors.white,
                                  backgroundColor: Colors.red,
                                  snackPosition: SnackPosition.BOTTOM);
                            },
                          ),
                          CupertinoActionSheetAction(
                            child: const Text('Batal'),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                          )
                        ],
                      );
                    },
                  );
                })
          ],
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: jenisSurat.toLowerCase() != 'penerima'
                        ? Colors.blue
                        : Colors.green,
                    borderRadius: BorderRadius.vertical(
                      top: Radius.circular(10),
                      // bottom: Radius.circular(10),
                    ),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.grey[500],
                          blurRadius: 4,
                          offset: Offset(2, 0))
                    ],
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: 10.0, right: 10.0, top: 7.0, bottom: 7.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          jenisSurat.toUpperCase() + ' SURAT',
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                        // Row(
                        //   children: [
                        //     Text(
                        //       'Lihat Foto Bukti',
                        //       style: TextStyle(color: Colors.white),
                        //     ),
                        //     // Icon(
                        //     //   Icons.image,
                        //     //   color: Colors.white,
                        //     // ),
                        //     Icon(
                        //       Icons.chevron_right,
                        //       color: Colors.white,
                        //     ),
                        //   ],
                        // )
                      ],
                    ),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.vertical(
                      // top: Radius.circular(10),
                      bottom: Radius.circular(10),
                    ),
                    // gradient: LinearGradient(
                    //     colors: listItem.nomorSurat == '---'
                    //         ? [Colors.orange, Colors.white]
                    //         : [Colors.blue, Colors.white],
                    //     begin: Alignment(-0.95, 0),
                    //     end: Alignment(-0.949, 0)),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.grey[500],
                          blurRadius: 4,
                          offset: Offset(0, 2))
                    ],
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: 10.0, right: 10.0, top: 5.0, bottom: 5.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(nomorSurat ?? '-',
                                style: TextStyle(fontSize: 14)),
                            Text(tanggalSurat, style: TextStyle(fontSize: 14)),
                          ],
                        ),
                        SizedBox(height: 10),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Nama Pengirim'),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Icon(Icons.forward_to_inbox),
                                SizedBox(width: 5),
                                Expanded(
                                  child: Text(
                                    namaPengirim ?? '',
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                        SizedBox(height: 10),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Nama Penerima'),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Icon(Icons.mark_email_read_outlined),
                                SizedBox(width: 5),
                                Expanded(
                                  child: Text(
                                    namaPenerima ?? '',
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                        SizedBox(height: 10),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Instansi'),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Icon(Icons.blur_on),
                                SizedBox(width: 5),
                                Expanded(
                                  child: Text(
                                    instansi ?? '',
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 10),
                penerimaSurat == null || penerimaSurat.length == 0
                    ? SizedBox.shrink()
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Foto Penerima',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Container(
                            height: 150,
                            child: ListView.separated(
                              scrollDirection: Axis.horizontal,
                              shrinkWrap: true,
                              itemCount: penerimaSurat != null &&
                                      penerimaSurat.isNotEmpty
                                  ? penerimaSurat.length
                                  : 0,
                              itemBuilder: (BuildContext context, int index) {
                                return penerimaSurat.isEmpty ||
                                        penerimaSurat == null
                                    ? SizedBox.shrink()
                                    : FullScreenWidget(
                                        child: InteractiveViewer(
                                          child: Center(
                                            child: Hero(
                                              tag: penerimaSurat
                                                  .map((e) => e)
                                                  .toList()[index],
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    color: Colors.white,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10),
                                                    boxShadow: [
                                                      BoxShadow(
                                                        color: Colors.grey
                                                            .withOpacity(0.5),
                                                        spreadRadius: 2,
                                                        blurRadius: 3,
                                                        offset: Offset(0,
                                                            1), // changes position of shadow
                                                      ),
                                                    ],
                                                  ),
                                                  child: ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8.0),
                                                    child: Image.network(
                                                      penerimaSurat
                                                          .map((e) => e)
                                                          .toList()[index],
                                                      fit: BoxFit.fitHeight,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      );
                              },
                              separatorBuilder:
                                  (BuildContext context, int index) =>
                                      const Divider(),
                            ),
                          ),
                        ],
                      ),
                ttd.isEmpty || ttd == null
                    ? SizedBox.shrink()
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Foto Tanda Tangan Penerima',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Row(
                            children: [
                              SizedBox(
                                height: 150,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: FullScreenWidget(
                                    child: InteractiveViewer(
                                      child: Center(
                                        child: Hero(
                                          tag: ttd[0],
                                          child: Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Container(
                                              decoration: BoxDecoration(
                                                color: Colors.white,
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                                boxShadow: [
                                                  BoxShadow(
                                                    color: Colors.grey
                                                        .withOpacity(0.5),
                                                    spreadRadius: 2,
                                                    blurRadius: 3,
                                                    offset: Offset(0,
                                                        1), // changes position of shadow
                                                  ),
                                                ],
                                              ),
                                              child: ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(8.0),
                                                child: Image.network(
                                                  ttd[0],
                                                  fit: BoxFit.fitHeight,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                fotoSurat == null || fotoSurat.length == 0
                    ? SizedBox.shrink()
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Foto Surat',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Container(
                            height: 150,
                            child: ListView.separated(
                              scrollDirection: Axis.horizontal,
                              shrinkWrap: true,
                              itemCount:
                                  fotoSurat != null && fotoSurat.isNotEmpty
                                      ? fotoSurat.length
                                      : 0,
                              itemBuilder: (BuildContext context, int index) {
                                return fotoSurat.isEmpty || fotoSurat == null
                                    ? SizedBox.shrink()
                                    : FullScreenWidget(
                                        child: InteractiveViewer(
                                          child: Center(
                                            child: Hero(
                                              tag: fotoSurat
                                                  .map((e) => e)
                                                  .toList()[index],
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    color: Colors.white,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10),
                                                    boxShadow: [
                                                      BoxShadow(
                                                        color: Colors.grey
                                                            .withOpacity(0.5),
                                                        spreadRadius: 2,
                                                        blurRadius: 3,
                                                        offset: Offset(0,
                                                            1), // changes position of shadow
                                                      ),
                                                    ],
                                                  ),
                                                  child: ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8.0),
                                                    child: Image.network(
                                                      fotoSurat
                                                          .map((e) => e)
                                                          .toList()[index],
                                                      fit: BoxFit.fitHeight,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      );
                              },
                              separatorBuilder:
                                  (BuildContext context, int index) =>
                                      const Divider(),
                            ),
                          ),
                        ],
                      ),
              ],
            ),
          ),
        ));
  }
}
