import 'package:cak_surdin/controllers/SuratController.dart';
import 'package:cak_surdin/ui/CustomLoading.dart';
import 'package:cak_surdin/ui/DaftarSuratMasuk/DetailSurat/DetailSuratMasuk.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:line_icons/line_icon.dart';
import 'package:line_icons/line_icons.dart';

class SuratMasuk extends StatefulWidget {
  const SuratMasuk({Key key, @required this.type, @required this.typetype})
      : super(key: key);

  final String type;
  final String typetype;

  @override
  _SuratMasukState createState() => _SuratMasukState();
}

class _SuratMasukState extends State<SuratMasuk> {
  final suratController = Get.put(SuratController());

  @override
  void initState() {
    suratController.fetchSuratMasuk(widget.type);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GetX<SuratController>(
      init: suratController,
      builder: (SuratController controller) {
        if (controller.suratMasukModel.value.list != null) {
          return controller.suratMasukModel.value.list.isEmpty
              ? Center(
                  child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Icon(
                      LineIcons.mailBulk,
                      size: 100,
                      color: Colors.grey,
                    ),
                    Text(
                      'Tidak ada data',
                      style: TextStyle(color: Colors.grey),
                    )
                  ],
                ))
              : RefreshIndicator(
                  onRefresh: () async =>
                      await suratController.fetchSuratMasuk(widget.type),
                  child: ListView.builder(
                    // physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: controller.suratMasukModel.value.list.length,
                    itemBuilder: (BuildContext context, int index) {
                      var listItem =
                          controller.suratMasukModel.value.list[index];

                      return Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 5, horizontal: 5),
                        child: InkWell(
                          onTap: () => Get.to(() => DetailSuratMasuk(
                                nomorSurat: listItem.nomorSurat,
                                tanggalSurat: listItem.tglSurat != null
                                    ? controller.formatter
                                        .format(listItem.tglSurat)
                                    : '',
                                perihal: listItem.perihal,
                                type: widget.typetype,
                                suratMasukID: listItem.suratMasukId,
                                instansi: listItem.instansiAsal,
                                typeSurat: listItem.type,
                              )),
                          child: Container(
                              decoration: BoxDecoration(
                                  color: Colors.red,
                                  borderRadius: BorderRadius.circular(10),
                                  gradient: LinearGradient(
                                      colors: listItem.nomorSurat == '---'
                                          ? [Colors.orange, Colors.white]
                                          : [Colors.blue, Colors.white],
                                      begin: Alignment(-0.95, 0),
                                      end: Alignment(-0.949, 0)),
                                  boxShadow: [
                                    BoxShadow(
                                        color: Colors.grey[500],
                                        blurRadius: 4,
                                        offset: Offset(0, 2))
                                  ]),
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    left: 15.0,
                                    right: 10.0,
                                    top: 5.0,
                                    bottom: 5.0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(listItem.nomorSurat ?? '',
                                            style: TextStyle(fontSize: 14)),
                                        Text(
                                            listItem.tglSurat != null
                                                ? controller.formatter
                                                    .format(listItem.tglSurat)
                                                : '',
                                            style: TextStyle(fontSize: 14)),
                                      ],
                                    ),
                                    SizedBox(height: 10),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Icon(Icons.forward_to_inbox),
                                        SizedBox(width: 5),
                                        Expanded(
                                          child: Text(
                                            listItem.instansiAsal ?? '',
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.w600),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Text(
                                      listItem.perihal ?? '',
                                      style: TextStyle(color: Colors.black45),
                                    ),
                                    SizedBox(height: 5),
                                    // Row(
                                    //   mainAxisAlignment: MainAxisAlignment.end,
                                    //   crossAxisAlignment: CrossAxisAlignment.end,
                                    //   children: [
                                    //     Text(
                                    //       listItem.typeSurat.toString() ?? '',
                                    //       style: TextStyle(color: Colors.black45),
                                    //       textAlign: TextAlign.end,
                                    //     ),
                                    //   ],
                                    // ),
                                  ],
                                ),
                              )),
                        ),
                      );
                    },
                  ),
                );
        } else {
          return Center(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(
                LineIcons.mailBulk,
                size: 100,
                color: Colors.grey,
              ),
              Text(
                'Data Kosong',
                style: TextStyle(color: Colors.grey),
              )
            ],
          ));
        }
      },
    );
  }
}
