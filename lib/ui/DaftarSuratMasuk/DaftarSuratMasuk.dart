import 'package:cak_surdin/controllers/HomeController.dart';
import 'package:cak_surdin/controllers/SuratController.dart';
import 'package:cak_surdin/ui/DaftarSuratMasuk/SuratMasuk.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DaftarSuratMasuk extends StatefulWidget {
  const DaftarSuratMasuk({Key key, @required this.title, @required this.type})
      : super(key: key);

  final String title;
  final String type;

  @override
  _DaftarSuratMasukState createState() => _DaftarSuratMasukState();
}

class _DaftarSuratMasukState extends State<DaftarSuratMasuk>
    with TickerProviderStateMixin {
  final homeController = Get.put(HomeController());
  final suratController = Get.put(SuratController());
  final TextEditingController searchController = TextEditingController();
  TabController _tabController;
  int tabIndex = 0;

  @override
  void initState() {
    tabListener();
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  void tabListener() async {
    _tabController =
        TabController(vsync: this, length: homeController.homeTabs.length);
    _tabController.addListener(_handleTabSelection);
  }

  void _handleTabSelection() async {
    if (_tabController.indexIsChanging) {
      switch (_tabController.index) {
        case 0:
          setState(() => tabIndex = _tabController.index);
          print('case ${tabIndex}');
          break;
        case 1:
          setState(() => tabIndex = _tabController.index);
          print('case ${tabIndex}');
          break;
        case 2:
          setState(() => tabIndex = _tabController.index);
          print('case ${tabIndex}');
          break;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: homeController.homeTabs.length,
      child: Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
          bottom: TabBar(
            controller: _tabController,
            indicatorColor: Colors.orange,
            indicatorWeight: 4.0,
            tabs: homeController.homeTabs,
          ),
        ),
        body: TabBarView(controller: _tabController, children: <Widget>[
          SuratMasuk(
            type: 'surat_masuk',
            typetype: widget.type,
          ),
          SuratMasuk(
            type: 'radiogram',
            typetype: widget.type,
          ),
          SuratMasuk(
            type: 'nota_dinas',
            typetype: widget.type,
          ),
        ]),
        floatingActionButton: _tabController.index == 0
            ? FloatingActionButton(
                child: Icon(CupertinoIcons.search),
                onPressed: () {
                  suratController.showBottomSearch(context, 'surat_masuk');
                },
              )
            : _tabController.index == 1
                ? FloatingActionButton(
                    child: Icon(CupertinoIcons.radiowaves_left),
                    onPressed: () {
                      suratController.showBottomSearch(context, 'radiogram');
                    },
                  )
                : FloatingActionButton(
                    child: Icon(CupertinoIcons.crop),
                    onPressed: () {
                      suratController.showBottomSearch(context, 'nota_dinas');
                    },
                  ),
      ),
    );
  }
}
