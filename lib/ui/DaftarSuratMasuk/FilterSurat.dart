import 'package:cak_surdin/controllers/SuratController.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class FilterSurat extends StatefulWidget {
  const FilterSurat({
    Key key,
    @required this.jenisSurat,
  }) : super(key: key);

  final String jenisSurat;

  @override
  _FilterSuratState createState() => _FilterSuratState();
}

class _FilterSuratState extends State<FilterSurat> {
  final suratController = Get.put(SuratController());
  final TextEditingController nomorSuratController = TextEditingController();
  final TextEditingController perihalController = TextEditingController();
  DateTime dateTime = DateTime.now();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'Filter ${widget.jenisSurat == 'surat_masuk' ? 'Surat Masuk'.toUpperCase() : widget.jenisSurat == 'radiogram' ? 'Radiogram'.toUpperCase() : 'Nota Dinas'.toUpperCase()}'
                  .toUpperCase(),
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                  fontStyle: FontStyle.italic),
            ),
            SizedBox(
              height: 10,
            ),
            CupertinoSearchTextField(
              padding: EdgeInsets.all(15),
              controller: nomorSuratController,
              placeholder: 'nomor surat',
              onChanged: (value) {
                suratController.rxNomorSurat.value = value;
                print("nomor text: " + value);
              },
            ),
            SizedBox(
              height: 10,
            ),
            CupertinoSearchTextField(
              padding: EdgeInsets.all(15),
              controller: perihalController,
              placeholder: 'perihal',
              onChanged: (value) {
                suratController.rxPerihal.value = value;
                print("perihal text: " + value);
              },
            ),
            SizedBox(
              height: 10,
            ),
            Obx(
              () => MaterialButton(
                color: Colors.grey.shade200,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Icon(Icons.date_range),
                    SizedBox(
                      width: 5,
                    ),
                    Text(suratController.rxDate.value.isNotEmpty
                        ? suratController.filterFormatter.format(
                            DateTime.parse(suratController.rxDate.value))
                        : 'Rentang Tanggal Surat'),
                  ],
                ),
                onPressed: () => showCupertinoModalPopup(
                  barrierDismissible: true,
                  context: context,
                  builder: (BuildContext context) {
                    return CupertinoActionSheet(
                      actions: [
                        SizedBox(
                          height: 180,
                          child: CupertinoDatePicker(
                            minimumYear: 2011,
                            maximumYear: DateTime.now().year,
                            initialDateTime: dateTime,
                            mode: CupertinoDatePickerMode.date,
                            onDateTimeChanged: (dateTime) {
                              setState(
                                () {
                                  suratController.rxDate.value =
                                      dateTime.toString();
                                  // print(this.dateTime);
                                },
                              );
                            },
                          ),
                        ),
                      ],
                      cancelButton: CupertinoActionSheetAction(
                          onPressed: () {
                            print(suratController.rxDate.value);
                            Get.back();
                          },
                          child: Text('Done')),
                    );
                  },
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            InkWell(
              onTap: () async {
                await suratController.fetchBottomSearch(
                    suratController.rxNomorSurat.value,
                    suratController.rxPerihal.value,
                    suratController.rxDate.value,
                    widget.jenisSurat);
                Get.back();
              },
              child: Container(
                width: double.infinity,
                decoration: BoxDecoration(
                  color: Colors.blue.withOpacity(0.8),
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 2,
                      blurRadius: 5,
                      offset: Offset(0, 1), // changes position of shadow
                    ),
                  ],
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        CupertinoIcons.search,
                        color: Colors.white,
                      ),
                      SizedBox(
                        width: 5,
                        height: 35,
                      ),
                      Text(
                        'Temukan Surat',
                        style: TextStyle(
                            fontWeight: FontWeight.w500, color: Colors.white),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
