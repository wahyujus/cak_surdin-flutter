import 'package:cak_surdin/controllers/AuthController.dart';
import 'package:cak_surdin/controllers/ProfileController.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:line_icons/line_icons.dart';

class ProfilPengguna extends StatefulWidget {
  const ProfilPengguna(
      {Key key, @required this.title, @required this.pageIndex})
      : super(key: key);

  final String title;
  final int pageIndex;

  @override
  _ProfilPenggunaState createState() => _ProfilPenggunaState();
}

class _ProfilPenggunaState extends State<ProfilPengguna> {
  final authController = Get.put(AuthController());
  final profileController = Get.put(ProfileController());

  final TextEditingController namaLengkapController = TextEditingController();
  final TextEditingController usernameController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController confirmPasswordController =
      TextEditingController();
  final TextEditingController noHpController = TextEditingController();

  GetStorage box = GetStorage();

  @override
  void initState() {
    startUp();
    super.initState();
  }

  void startUp() async {
    setState(() {
      namaLengkapController.text =
          authController.loginModel.value.data.namaUser;
      usernameController.text = authController.loginModel.value.data.namaLogin;
      noHpController.text = authController.loginModel.value.data.noTlp;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Obx(() => SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              child: Column(
                children: [
                  TextField(
                    controller: namaLengkapController,
                    decoration: InputDecoration(
                      prefixIcon: Icon(LineIcons.identificationBadge),
                      labelText: 'Nama Lengkap',
                    ),
                  ),
                  TextField(
                    controller: usernameController,
                    decoration: InputDecoration(
                      prefixIcon: Icon(LineIcons.userAlt),
                      labelText: 'Username',
                    ),
                  ),
                  TextField(
                    controller: passwordController,
                    decoration: InputDecoration(
                      prefixIcon: Icon(CupertinoIcons.padlock),
                      suffixIcon: IconButton(
                        onPressed: () {
                          profileController.isPasswordHide.toggle();
                        },
                        icon: profileController.isPasswordHide.value
                            ? Icon(
                                CupertinoIcons.eye_fill,
                              )
                            : Icon(
                                CupertinoIcons.eye_slash_fill,
                              ),
                      ),
                      labelText: 'Password',
                    ),
                    obscureText:
                        profileController.isPasswordHide.value ? false : true,
                  ),
                  TextField(
                    controller: confirmPasswordController,
                    decoration: InputDecoration(
                      prefixIcon: Icon(CupertinoIcons.padlock),
                      suffixIcon: IconButton(
                        onPressed: () {
                          profileController.isPasswordHide.toggle();
                        },
                        icon: profileController.isPasswordHide.value
                            ? Icon(
                                CupertinoIcons.eye_fill,
                              )
                            : Icon(
                                CupertinoIcons.eye_slash_fill,
                              ),
                      ),
                      labelText: 'Password',
                    ),
                    obscureText:
                        profileController.isPasswordHide.value ? false : true,
                  ),
                  TextField(
                    controller: noHpController,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      prefixIcon: Icon(CupertinoIcons.phone),
                      labelText: 'No. HP',
                    ),
                    obscureText: false,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    width: double.maxFinite,
                    child: ElevatedButton(
                      style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(Colors.green[500])),
                      onPressed: () {
                        // if (usernameController.text != null) {
                        if (usernameController.text != null &&
                                passwordController.text !=
                                    confirmPasswordController.text ||
                            confirmPasswordController.text.isEmpty ||
                            passwordController.text.isEmpty) {
                          EasyLoading.showError('password is empty');
                        } else {
                          box.write('username', usernameController.text);
                          box.write('password', passwordController.text);
                          profileController.updateUserData(
                            authController.loginModel.value.data.userLoginId,
                            usernameController.text,
                            namaLengkapController.text,
                            passwordController.text,
                            noHpController.text,
                          );
                        }
                        // }
                      },
                      child: Text('Ubah Profile'),
                    ),
                  )
                ],
              ),
            ),
          )),
    );
  }
}
