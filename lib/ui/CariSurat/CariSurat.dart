import 'package:cak_surdin/controllers/CariSuratController.dart';
import 'package:cak_surdin/ui/DaftarSuratMasuk/DetailSurat/DetailSuratMasuk.dart';
import 'package:clipboard/clipboard.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:line_icons/line_icons.dart';

class CariSurat extends StatefulWidget {
  const CariSurat({Key key}) : super(key: key);

  @override
  _CariSuratState createState() => _CariSuratState();
}

class _CariSuratState extends State<CariSurat> {
  final cariSuratController = Get.put(CariSuratController());
  final TextEditingController searchController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: CloseButton(
          onPressed: () {
            EasyLoading.dismiss();
            Get.back();
          },
        ),
        title: Text('Cari Surat'),
      ),
      body: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              child: TextField(
                controller: searchController,
                decoration: InputDecoration(
                  hintText: 'ex : 23/PD-DMI/III/2021',
                  suffixIcon: IconButton(
                      onPressed: () {
                        FlutterClipboard.paste().then((value) {
                          // Do what ever you want with the value.
                          setState(() {
                            searchController.text = value;
                          });
                          print(value);
                        });
                      },
                      icon: Icon(Icons.paste)),
                  prefixIcon: Icon(Icons.search),
                ),
                onSubmitted: (value) async {
                  await cariSuratController.fetchCariSurat(value);
                },
              )
              // CupertinoSearchTextField(
              //   suffixIcon: Icon(Icons.paste),
              //   padding: EdgeInsets.all(10),
              //   controller: searchController,
              //   placeholder: 'nomor surat',
              //   onSubmitted: (value) {
              //     cariSuratController.fetchCariSurat(value);
              //     print("Submitted text: " + value);
              //   },
              // ),
              ),
          Expanded(
            child: GetX<CariSuratController>(
              init: cariSuratController,
              builder: (CariSuratController controller) {
                if (controller.cariSuratModelStream.list != null) {
                  if (controller.cariSuratModelStream.list.isEmpty) {
                    return Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SizedBox(
                              height: 200,
                              width: 200,
                              child: Image.asset(
                                'assets/001-search.png',
                              )),
                          Text('Nomer Surat tidak ada')
                        ],
                      ),
                    );
                  } else {
                    return ListView.builder(
                      shrinkWrap: true,
                      itemCount: controller.cariSuratModelStream.list.length,
                      itemBuilder: (BuildContext context, int index) {
                        var listItem =
                            controller.cariSuratModelStream.list[index];
                        return Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 5, horizontal: 5),
                          child: InkWell(
                            onTap: () {
                              Get.to(() => DetailSuratMasuk(
                                    nomorSurat: listItem.nomorSurat,
                                    tanggalSurat: listItem.tglSurat != null
                                        ? controller.formatter
                                            .format(listItem.tglSurat)
                                        : '',
                                    perihal: listItem.perihal,
                                    type: '',
                                    suratMasukID: listItem.suratMasukId,
                                    instansi: listItem.instansiAsal,
                                    typeSurat: listItem.type,
                                  ));
                            },
                            child: Container(
                                decoration: BoxDecoration(
                                    color: Colors.red,
                                    borderRadius: BorderRadius.circular(10),
                                    gradient: LinearGradient(
                                        colors: listItem.nomorSurat == '---'
                                            ? [Colors.orange, Colors.white]
                                            : [Colors.blue, Colors.white],
                                        begin: Alignment(-0.95, 0),
                                        end: Alignment(-0.949, 0)),
                                    boxShadow: [
                                      BoxShadow(
                                          color: Colors.grey[500],
                                          blurRadius: 4,
                                          offset: Offset(0, 2))
                                    ]),
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 15.0,
                                      right: 10.0,
                                      top: 5.0,
                                      bottom: 5.0),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(listItem.nomorSurat ?? '',
                                              style: TextStyle(fontSize: 14)),
                                          Text(
                                              listItem.tglSurat != null
                                                  ? controller.formatter
                                                      .format(listItem.tglSurat)
                                                  : '',
                                              style: TextStyle(fontSize: 14)),
                                        ],
                                      ),
                                      SizedBox(height: 10),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Icon(Icons.forward_to_inbox),
                                          SizedBox(width: 5),
                                          Expanded(
                                            child: Text(
                                              listItem.instansiAsal ?? '',
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.w600),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Text(
                                        listItem.perihal ?? '',
                                        style: TextStyle(color: Colors.black45),
                                      ),
                                      SizedBox(height: 5),
                                      // Row(
                                      //   mainAxisAlignment: MainAxisAlignment.end,
                                      //   crossAxisAlignment: CrossAxisAlignment.end,
                                      //   children: [
                                      //     Text(
                                      //       listItem.typeSurat.toString() ?? '',
                                      //       style: TextStyle(color: Colors.black45),
                                      //       textAlign: TextAlign.end,
                                      //     ),
                                      //   ],
                                      // ),
                                    ],
                                  ),
                                )),
                          ),
                        );
                        // Column(
                        //   children: [
                        //     CupertinoSlidingSegmentedControl(
                        //       children: controller.myTabs,
                        //       thumbColor: Colors.white,
                        //       groupValue:
                        //           controller.segmentedControlGroupValue.value,
                        //       backgroundColor: Colors.grey.shade300,
                        //       onValueChanged: (value) {
                        //         controller.segmentedControlGroupValue.value =
                        //             value;
                        //         print(value);
                        //       },
                        //     ),
                        //     controller.segmentedControlGroupValue.value == 0
                        //         ? ListTile(
                        //             title: Column(
                        //               crossAxisAlignment:
                        //                   CrossAxisAlignment.start,
                        //               children: [
                        //                 Text(listItem.typeSurat ?? ''),
                        //                 SizedBox(
                        //                   height: 10,
                        //                 ),
                        //                 Text('Nomor Surat Masuk'),
                        //                 Text(listItem.nomorSurat ?? ''),
                        //                 SizedBox(
                        //                   height: 10,
                        //                 ),
                        //                 Text('Tanggal Surat'),
                        //                 Text(controller.formatter
                        //                         .format(listItem.tglSurat) ??
                        //                     ''),
                        //                 SizedBox(
                        //                   height: 10,
                        //                 ),
                        //                 Text('Perihal'),
                        //                 Text(listItem.perihal ?? ''),
                        //                 SizedBox(
                        //                   height: 10,
                        //                 ),
                        //                 Text('Kepada'),
                        //                 Text(listItem.kepada ?? ''),
                        //                 SizedBox(
                        //                   height: 10,
                        //                 ),
                        //                 Text('Instansi Asal'),
                        //                 Text(listItem.instansiAsal ?? ''),
                        //                 SizedBox(
                        //                   height: 10,
                        //                 ),
                        //                 Text('Tujuan Surat'),
                        //                 Text(listItem.satkerTujuanId ?? ''),
                        //               ],
                        //             ),
                        //           )
                        //         : ListTile(
                        //             title: Text('disposisi'),
                        //           )
                        //   ],
                        // );
                      },
                    );
                  }
                } else {
                  return Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          LineIcons.searchengin,
                          color: Colors.grey,
                          size: 200,
                        ),
                        Text(
                          'Cari Nomer Surat',
                          style: TextStyle(fontStyle: FontStyle.italic),
                        )
                      ],
                    ),
                  );
                }
              },
            ),
          )
        ],
      ),
    );
  }
}
