import 'package:cak_surdin/controllers/AuthController.dart';
import 'package:cak_surdin/controllers/SuratController.dart';
import 'package:cak_surdin/ui/DaftarSuratMasuk/DetailSurat/CreateResi.dart';
import 'package:cak_surdin/ui/DaftarSuratMasuk/DetailSurat/DetailTandaTerima.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:line_icons/line_icons.dart';

class ResiTandaTerima extends StatefulWidget {
  // const ResiTandaTerima({Key? key}) : super(key: key);

  final String title;
  final String type;

  const ResiTandaTerima({
    Key key,
    @required this.title,
    @required this.type,
  }) : super(key: key);

  @override
  _ResiTandaTerimaState createState() => _ResiTandaTerimaState();
}

class _ResiTandaTerimaState extends State<ResiTandaTerima> {
  final authController = Get.put(AuthController());
  final suratController = Get.put(SuratController());
  GetStorage box = GetStorage();

  @override
  void initState() {
    suratController.fetchTandaTerimaPenerima(
        authController.loginModel.value.data.userLoginId, widget.type);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('${widget.type.toUpperCase()} Resi Tanda Terima'),
          backgroundColor: Colors.green,
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.green,
          child: Icon(LineIcons.receipt),
          onPressed: () {
            Get.to(() => CreateResi(
                  type: widget.type,
                ));
          },
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                GetX<SuratController>(
                  init: suratController,
                  builder: (SuratController controller) {
                    if (controller.suratMasukDetailModel.value.list != null &&
                        controller
                            .suratMasukDetailModel.value.list.isNotEmpty) {
                      return RefreshIndicator(
                        onRefresh: () async {
                          // await suratController.fetchSuratMasuk(type);
                        },
                        child: ListView.builder(
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemCount: controller
                              .suratMasukDetailModel.value.list.length,
                          itemBuilder: (BuildContext context, int index) {
                            var listItem = controller
                                .suratMasukDetailModel.value.list[index];
                            return Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 5, horizontal: 5),
                              child: InkWell(
                                onTap: () {
                                  Get.to(() => DetailTandaTerima(
                                        instansi: listItem.instansi,
                                        jenisSurat: listItem.type,
                                        namaPenerima: listItem.namaPenerima,
                                        namaPengirim: listItem.namaPengirim,
                                        nomorSurat: listItem.nomorSurat ?? '-',
                                        tanggalSurat: controller.detailFormatter
                                            .format(listItem.createdAt)
                                            .toString(),
                                        id: listItem.id,
                                        suratMasukID: listItem.suratMasukId,
                                        penerimaSurat: listItem.fotoPenerima,
                                        ttd: listItem.fotoTtd,
                                        typeSurat: listItem.type,
                                        fotoSurat: listItem.fotoSurat,
                                      ));
                                },
                                child: Column(
                                  children: [
                                    Container(
                                      width: double.infinity,
                                      decoration: BoxDecoration(
                                        color: listItem.type.toLowerCase() !=
                                                'penerima'
                                            ? Colors.blue
                                            : Colors.green,
                                        borderRadius: BorderRadius.vertical(
                                          top: Radius.circular(10),
                                          // bottom: Radius.circular(10),
                                        ),
                                        boxShadow: [
                                          BoxShadow(
                                              color: Colors.grey[500],
                                              blurRadius: 4,
                                              offset: Offset(2, 0))
                                        ],
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.only(
                                            left: 10.0,
                                            right: 10.0,
                                            top: 7.0,
                                            bottom: 7.0),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              listItem.type.toUpperCase() +
                                                  ' SURAT',
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            Row(
                                              children: [
                                                Text(
                                                  'Lihat Foto Bukti',
                                                  style: TextStyle(
                                                      color: Colors.white),
                                                ),
                                                // Icon(
                                                //   Icons.image,
                                                //   color: Colors.white,
                                                // ),
                                                Icon(
                                                  Icons.chevron_right,
                                                  color: Colors.white,
                                                ),
                                              ],
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                    Container(
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.vertical(
                                          // top: Radius.circular(10),
                                          bottom: Radius.circular(10),
                                        ),
                                        // gradient: LinearGradient(
                                        //     colors: listItem.nomorSurat == '---'
                                        //         ? [Colors.orange, Colors.white]
                                        //         : [Colors.blue, Colors.white],
                                        //     begin: Alignment(-0.95, 0),
                                        //     end: Alignment(-0.949, 0)),
                                        boxShadow: [
                                          BoxShadow(
                                              color: Colors.grey[500],
                                              blurRadius: 4,
                                              offset: Offset(0, 2))
                                        ],
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.only(
                                            left: 10.0,
                                            right: 10.0,
                                            top: 5.0,
                                            bottom: 5.0),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Text(listItem.nomorSurat ?? '-',
                                                    style: TextStyle(
                                                        fontSize: 14)),
                                                Text(
                                                    controller.detailFormatter
                                                        .format(
                                                            listItem.createdAt)
                                                        .toString(),
                                                    style: TextStyle(
                                                        fontSize: 14)),
                                              ],
                                            ),
                                            SizedBox(height: 10),
                                            Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text('Nama Pengirim'),
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  children: [
                                                    Icon(
                                                        Icons.forward_to_inbox),
                                                    SizedBox(width: 5),
                                                    Expanded(
                                                      child: Text(
                                                        listItem.namaPengirim ??
                                                            '',
                                                        style: TextStyle(
                                                            color: Colors.black,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w600),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                            SizedBox(height: 10),
                                            Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text('Nama Penerima'),
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  children: [
                                                    Icon(Icons
                                                        .mark_email_read_outlined),
                                                    SizedBox(width: 5),
                                                    Expanded(
                                                      child: Text(
                                                        listItem.namaPenerima ??
                                                            '',
                                                        style: TextStyle(
                                                            color: Colors.black,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w600),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                            SizedBox(height: 10),
                                            Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text('Instansi'),
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  children: [
                                                    Icon(Icons.blur_on),
                                                    SizedBox(width: 5),
                                                    Expanded(
                                                      child: Text(
                                                        listItem.instansi ?? '',
                                                        style: TextStyle(
                                                            color: Colors.black,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w600),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            );
                          },
                        ),
                      );
                    } else {
                      return Center(
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(
                            LineIcons.mailBulk,
                            size: 100,
                            color: Colors.grey,
                          ),
                          Text(
                            'Data Kosong',
                            style: TextStyle(color: Colors.grey),
                          )
                        ],
                      ));
                    }
                  },
                )
              ],
            ),
          ),
        ));
  }
}
