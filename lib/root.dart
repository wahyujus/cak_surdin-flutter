import 'package:cak_surdin/controllers/AuthController.dart';
import 'package:cak_surdin/ui/Home.dart';
import 'package:cak_surdin/ui/auth/Login.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class Root extends GetWidget<AuthController> {
  GetStorage box = GetStorage();
  @override
  Widget build(BuildContext context) {
    return controller.userToken.value == null ? Login() : Home();
  }
}
